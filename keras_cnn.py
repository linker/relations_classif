# processing texts, reading and creating labels
TEXT_DATA_DIR = "20_newsgroup"

import os

texts = []  # list of text samples
labels_index = {}  # dictionary mapping label name to numeric id
labels = []  # list of label ids corresponding to texts
# iterate through folders in data directory, folder for every type of news
for name in sorted(os.listdir(TEXT_DATA_DIR)):
    path = os.path.join(TEXT_DATA_DIR, name)
    if os.path.isdir(path):
        label_id = len(labels_index)
        # add the index to the name of the news type
        labels_index[name] = label_id
        for fname in sorted(os.listdir(path)):
            # name of each new is a number
            if fname.isdigit():
                fpath = os.path.join(path, fname)
                f = open(fpath)
                texts.append(f.read())
                f.close()
                labels.append(label_id)

print('Found %s texts.' % len(texts))


from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

MAX_NB_WORDS = 20000

# class for turning text into the sequence of word indexes, where each index corresponds to the rank of the word
# nb_words will restrict the number of words to work with, the first most frequent nb_words
tokenizer = Tokenizer(nb_words=MAX_NB_WORDS)
# create indexes
tokenizer.fit_on_texts(texts)
# convert to sequences of indexes of the words
sequences = tokenizer.texts_to_sequences(texts)

# indexes (ranks) of words with words
word_index = tokenizer.word_index
print('Found %s unique tokens.' % len(word_index))

# length of the swquence that we are taking from every new text
MAX_SEQUENCE_LENGTH = 1000

# pads every sequnce to 1000, either truncates it or pads with 0s
data = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)

from keras.utils.np_utils import to_categorical
import numpy as np

# transform labels to one-hot arrays
labels = to_categorical(np.asarray(labels))
print('Shape of data tensor:', data.shape)
print('Shape of label tensor:', labels.shape)

# split the data into a training set and a validation set
VALIDATION_SPLIT = 0.2

# shuffle data and labels
indices = np.arange(data.shape[0])
np.random.shuffle(indices)
data = data[indices]
labels = labels[indices]
nb_validation_samples = int(VALIDATION_SPLIT * data.shape[0])

# split into training and validation set
x_train = data[:-nb_validation_samples]
y_train = labels[:-nb_validation_samples]
x_val = data[-nb_validation_samples:]
y_val = labels[-nb_validation_samples:]

GLOVE_DIR = "glove.6B"

# index with words as indeces and embedding as value
embeddings_index = {}
# take embeddings of length 100
f = open(os.path.join(GLOVE_DIR, 'glove.6B.100d.txt'))
for line in f:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()

print('Found %s word vectors.' % len(embeddings_index))

EMBEDDING_DIM = 100

# form an embedding matrix for each of the words in our text sequences
# if there is no embedding in GloVe then just leave zeros
# +1 for non embedded word
embedding_matrix = np.zeros((len(word_index) + 1, EMBEDDING_DIM))
for word, i in word_index.items():
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        # words not found in embedding index will be all-zeros.
        embedding_matrix[i] = embedding_vector

from keras.layers import Embedding, Dense, MaxPooling1D, Input, Flatten
from keras.layers import Convolution1D as Conv1D
from keras.models import Model

# +1 for non embedded word
# set weights to out embeddings matrix
# an Embedding layer should be fed sequences of integers, i.e. a 2D input of shape (samples, indices)
# an Embedding layer is capable of processing sequence of heterogenous length, if you don't pass an explicit input_length argument to the layer
# the output of the Embedding layer will be a 3D tensor of shape (samples, sequence_length, embedding_dim)
embedding_layer = Embedding(len(word_index) + 1,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=False)

# input are sequences, batchs of sequences
sequence_input = Input(shape=(MAX_SEQUENCE_LENGTH,), dtype='int32')
# transform sequences to matrices of embeddings
embedded_sequences = embedding_layer(sequence_input)
# 128 filters of size 5
x = Conv1D(128, 5, activation='relu')(embedded_sequences)
x = MaxPooling1D(5)(x)
x = Conv1D(128, 5, activation='relu')(x)
x = MaxPooling1D(5)(x)
x = Conv1D(128, 5, activation='relu')(x)
x = MaxPooling1D(35)(x)  # global max pooling
x = Flatten()(x)
x = Dense(128, activation='relu')(x)
preds = Dense(len(labels_index), activation='softmax')(x)

model = Model(sequence_input, preds)
model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['acc'])

# happy learning!
model.fit(x_train, y_train, validation_data=(x_val, y_val), nb_epoch=2, batch_size=128)

# just random embeddings
# embedding_layer = Embedding(len(word_index) + 1,
#                             EMBEDDING_DIM,
#                             input_length=MAX_SEQUENCE_LENGTH)
