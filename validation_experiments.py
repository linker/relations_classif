from relations_model.relations_cnn import RelationsCnn
from relations_model.semeval_read import SemevalRead
from relations_model.input_container import InputContainer
import numpy as np
import os

n_cross = 4
zero_position = 100
reader = SemevalRead(zero_position)
train_data, train_labels = reader.get_train_data()
splitted_train_data = np.split(np.asarray(train_data), n_cross)
splitted_train_labels = np.split(train_labels, n_cross)
test_data, test_labels = reader.get_test_data()

for i in range(n_cross):
  current_labels = np.array([])
  for j in np.delete(range(n_cross), i):
    current_labels = np.concatenate((current_labels, splitted_train_labels[j]))
  
  n = 0
  keys = ""
  for label in splitted_train_labels[i]:
    keys += str(n) + "\t" + label + "\n"
    n+=1
  test_keys_file = open("test_keys_file" + str(i) + ".txt", "w")
  test_keys_file.write(keys)
  test_keys_file.close()
  n = 0
  keys = ""
  for label in current_labels:
    keys += str(n) + "\t" + label + "\n"
    n+=1
  train_keys_file = open("train_keys_file" + str(i) + ".txt", "w")
  train_keys_file.write(keys)
  train_keys_file.close()

for d_emb_len in {30, 40, 50, 70}:
  for w_emb_len in {300, 400}:
    for emb in {'word2vec', 'glove', 'swivel'}:
      for i in range(n_cross):
        input_container = InputContainer()
        input_container.emb_name = emb
        input_container.emb_length = w_emb_len
        input_container.load_embeddings(load_full_vocab=False)

        current_train = np.empty((0,3))
        current_labels = np.array([])
        for j in np.delete(range(n_cross), i):
          current_train = np.concatenate((current_train, splitted_train_data[j]))
          current_labels = np.concatenate((current_labels, splitted_train_labels[j]))

        input_container.prepare_data(current_train, current_labels, splitted_train_data[i], splitted_train_labels[i])

        network = RelationsCnn(False)
        network.load_input(input_container, zero_position)
        network.init_model(distances_emb_length=d_emb_len, train_words_emb=True, train_distance_emb=True, filters_num=1000, window_size=3, regulize_rate=0.001)
        network.train(15, start_epoch=0, verbose=False, check_train_accuracy=True)
        network.save_model()
        network.form_evaluation_files()
        network.get_representative_trigrams()
