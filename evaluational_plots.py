from relations_model.relations_cnn import RelationsCnn
from keras.layers import Embedding, Dense, MaxPooling1D, Input, Flatten, Merge, Reshape, GlobalMaxPooling1D
from keras.layers import Convolution1D as Conv1D
from keras.regularizers import l2
from keras.models import Model, Sequential
import numpy as np

network = RelationsCnn(False)
network.prepare_input(emb_name='word2vec', emb_length=300, load_full_vocab=False, dataset_name='semeval', zero_position=100)
network.init_model(distances_emb_length=30, train_words_emb=True, train_distance_emb=True, filters_num=1000, window_size=3, regulize_rate=0.001)
network.model.load_weights("validated_best/weights_epochs15-16_201703031213.h5")

word_emb = Sequential()
word_emb.add(Embedding(network.input_container.vocab_length, network.input_container.emb_length))
d1_emb = Sequential()
d1_emb.add(Embedding(network.distances_vocab_length, network.distances_emb_length))
d2_emb = Sequential()
d2_emb.add(Embedding(network.distances_vocab_length, network.distances_emb_length))

conv_model = Sequential()
conv_model.add(Merge([word_emb, d1_emb, d2_emb], mode='concat'))
conv_model.add(Conv1D(network.filters_num, network.window_size, activation='tanh', border_mode='same', subsample_length=1, input_dim=network.full_emb_length))
conv_model.set_weights(network.model.get_weights())

repr_model = Sequential()
repr_model.add(Merge([word_emb, d1_emb, d2_emb], mode='concat'))
repr_model.add(Conv1D(network.filters_num, network.window_size, activation='tanh', border_mode='same', subsample_length=1, input_dim=network.full_emb_length))
repr_model.add(GlobalMaxPooling1D())
repr_model.set_weights(network.model.get_weights())

ex_number = 62
seq = network.input_container.test_sequences[ex_number]
l = len(seq)
text = network.input_container.test_data_texts[ex_number]
seq_input = [np.asarray(seq).reshape((1,l)), np.asarray(network.input_container.test_d1s[ex_number]).reshape((1,l)), np.asarray(network.input_container.test_d2s[ex_number]).reshape((1,l))]

convolution_output = np.squeeze(conv_model.predict(seq_input))
sentence_repr = np.squeeze(repr_model.predict(seq_input))
prediction = network.model.predict(seq_input).reshape(network.input_container.n_classes)

import matplotlib.pyplot as plt

# scores distribution
plt.rcParams.update({'font.size': 14})
plt.figure()
ax = plt.subplot(111)
quant = prediction.shape[0]
ax.bar(range(0,quant), prediction)
ax.set_xticks(map(lambda x: x + 0.5, range(0,quant)))
ax.set_xticklabels(network.input_container.labels_list, rotation=90)
ax.set_ylabel('Score')
plt.ylim(1.2 * prediction.min(), 1.2 * prediction.max())
plt.show()

# semantic values
predicted_class = prediction.argmax()
label_predicted = network.input_container.labels_list[predicted_class]
trigram_filters = {}
for wrd in range(len(text)):
  trigram_filters[wrd] = 0
for filter_row in convolution_output.T:
  trigram_index = filter_row.argmax()
  trigram_filters[trigram_index] += 1

print trigram_filters
print text

y_values = [x/1000.0 for x in trigram_filters.values()]
y_max = max(y_values)
plt.figure()
ax = plt.subplot(111)
plt.plot(y_values, marker='s')
ax.set_xticks(map(lambda x: x, range(0,l)))
ax.set_xticklabels(text, rotation=40)
plt.grid()
plt.ylim(-0.001, 1.2 * y_max)
plt.show()


#projector for embeddings
with open("class_embeddings.tsv", "w") as outp:
  for emb in network.model.layers[3].get_weights()[0].T:
    outp.write("\t".join(emb.astype('str')) + '\n')

with open("class_labels.tsv", "w") as outp:
  for class_name in network.input_container.labels_list:
    outp.write(class_name + '\n')


#plot dependencies on length of sentences amount of errors and correct answers
import numpy as np 
i = 0
correct = {key: 0 for key in np.arange(0, 210, 10)}
errors = {key: 0 for key in np.arange(0, 210, 10)}
amounts = {key: 0 for key in np.arange(0, 210, 10)}
for seq in network.input_container.test_sequences:
  l = len(seq)
  prediction = network.model.predict([np.asarray(seq).reshape((1,l)), np.asarray(network.input_container.test_d1s[i]).reshape((1,l)), np.asarray(network.input_container.test_d2s[i]).reshape((1,l))]).reshape(network.input_container.n_classes)
  prediction = network.preprocess_prediction(prediction)
  if l/10 < 20:
    index = 10 * (l/10 + 1)
  else:
    index = 200
  amounts[index] += 1
  if prediction.argmax() == network.input_container.test_labels[i].argmax():
    correct[index] += 1
  else:
    errors[index] += 1
  i+=1

for k in amounts:
  if amounts[k] > 0:
    correct[k] = (correct[k]*1.0) / (amounts[k]*1.0)
    errors[k] = (errors[k]*1.0) / (amounts[k]*1.0)

order = np.argsort(np.asarray(correct.keys()))
x1 = np.asarray(correct.keys())[order]
y1 = np.asarray(correct.values())[order]
x2 = np.asarray(errors.keys())[order]
y2 = np.asarray(errors.values())[order]

plt.figure()
ax = plt.subplot(111)
ax.set_xticks(np.arange(-10, 220, 10))
plt.ylim(-0.01, 1.2 * max(y2))
plt.xlim(-10, 210)
plt.plot(x1, y1, 'g^-', x2, y2, 'ro-')
plt.show()

#plot dependencies of distances between entities of correct and wrong answers
i = 0
correct = {key: 0 for key in np.arange(0, 210, 10)}
errors = {key: 0 for key in np.arange(0, 210, 10)}
amounts = {key: 0 for key in np.arange(0, 210, 10)}
for seq in network.input_container.test_sequences:
  l = len(seq)
  prediction = network.model.predict([np.asarray(seq).reshape((1,l)), np.asarray(network.input_container.test_d1s[i]).reshape((1,l)), np.asarray(network.input_container.test_d2s[i]).reshape((1,l))]).reshape(network.input_container.n_classes)
  prediction = network.preprocess_prediction(prediction)
  if not zero_position in network.input_container.test_d1s[i]:
    i+=1
    continue
  e1_ind = network.input_container.test_d1s[i].index(zero_position)
  betw_ent = abs(network.input_container.test_d2s[i][e1_ind] - zero_position)
  if betw_ent/10 < 20:
    index = 10 * (betw_ent/10 + 1)
  else:
    index = 200
  amounts[index] += 1
  if prediction.argmax() == network.input_container.test_labels[i].argmax():
    correct[index] += 1
  else:
    errors[index] += 1
  i+=1

for k in amounts:
  if amounts[k] > 0:
    correct[k] = (correct[k]*1.0) / (amounts[k]*1.0)
    errors[k] = (errors[k]*1.0) / (amounts[k]*1.0)

order = np.argsort(np.asarray(correct.keys()))
x1 = np.asarray(correct.keys())[order]
y1 = np.asarray(correct.values())[order]
x2 = np.asarray(errors.keys())[order]
y2 = np.asarray(errors.values())[order]

plt.figure()
ax = plt.subplot(111)
ax.set_xticks(np.arange(-10, 220, 10))
plt.ylim(-0.01, 1.2 * max(y2))
plt.xlim(-10, 210)
plt.plot(x1, y1, 'g^-', x2, y2, 'ro-')
plt.show()


#plot differences in correctly recognized testing examples
# network with distant weights
dist_correct = []
i = 0
for seq in network.input_container.test_sequences:
    l = len(seq)
    prediction = network.model.predict([np.asarray(seq).reshape((1,l)), np.asarray(network.input_container.test_d1s[i]).reshape((1,l)), np.asarray(network.input_container.test_d2s[i]).reshape((1,l))]).reshape(network.input_container.n_classes)
    prediction = network.preprocess_prediction(prediction)
    if prediction.argmax() == network.input_container.test_labels[i].argmax():
        dist_correct.append(1)
    else:
        dist_correct.append(0)
    i+=1

#network with supervised weights
superv_correct = []
i = 0
for seq in network.input_container.test_sequences:
    l = len(seq)
    prediction = network.model.predict([np.asarray(seq).reshape((1,l)), np.asarray(network.input_container.test_d1s[i]).reshape((1,l)), np.asarray(network.input_container.test_d2s[i]).reshape((1,l))]).reshape(network.input_container.n_classes)
    prediction = network.preprocess_prediction(prediction)
    if prediction.argmax() == network.input_container.test_labels[i].argmax():
        superv_correct.append(1)
    else:
        superv_correct.append(0)
    i+=1

dist_better = []
for i in range(len(dist_correct)):
    if superv_correct[i]==0 and dist_correct[i]==1:
        dist_better.append(i)

superv_better = []
for i in range(len(dist_correct)):
    if superv_correct[i]==1 and dist_correct[i]==0:
        superv_better.append(i)

dist_better_by_classes = {k:0 for k in network.input_container.labels_list}
for ind in dist_better:                                                    
    dist_better_by_classes[network.input_container.labels_list[network.input_container.test_labels[ind].argmax()]] += 1
superv_better_by_classes = {k:0 for k in network.input_container.labels_list}
for ind in superv_better:                                                    
    superv_better_by_classes[network.input_container.labels_list[network.input_container.test_labels[ind].argmax()]] += 1

plt.bar(range(len(dist_better_by_classes)), dist_better_by_classes.values(), 0.35, color='r')
plt.bar(np.asarray(range(len(superv_better_by_classes))) + 0.35, superv_better_by_classes.values(), 0.35, color='b')
plt.xticks(np.asarray(range(len(superv_better_by_classes)))+0.35, superv_better_by_classes.keys(), rotation=90)
plt.show()


# most frequent entities in the dataset per relation
k = 0
ent_dict = {k:{} for k in network.input_container.labels_list}
for t in network.input_container.train_data_texts:
  e1_ind = [i for i, x in enumerate(network.input_container.d1s[k]) if x==zero_position]
  e1 = '_'.join(np.asarray(t)[e1_ind])
  e2_ind = [i for i, x in enumerate(network.input_container.d2s[k]) if x==zero_position]
  e2 = '_'.join(np.asarray(t)[e2_ind])
  cur_label = network.input_container.labels_list[network.input_container.train_labels[k].argmax()]
  if ent_dict[cur_label].get(e1) == None:
    ent_dict[cur_label][e1] = 1
  else:
    ent_dict[cur_label][e1] += 1
  if ent_dict[cur_label].get(e2) == None:
    ent_dict[cur_label][e2] = 1
  else:
    ent_dict[cur_label][e2] += 1
  k+=1

for r in ent_dict:
  print r + ' ' + str(len(ent_dict[r]))
  print sorted(ent_dict[r].items(), key=operator.itemgetter(1), reverse=True)[0:11]


#get sets of entities
from sets import Set
k = 0
ent_dict = {k:Set([]) for k in network.input_container.labels_list}
for t in network.input_container.train_data_texts:
  e1_ind = [i for i, x in enumerate(network.input_container.d1s[k]) if x==zero_position]
  e1 = '_'.join(np.asarray(t)[e1_ind])
  e2_ind = [i for i, x in enumerate(network.input_container.d2s[k]) if x==zero_position]
  e2 = '_'.join(np.asarray(t)[e2_ind])
  cur_label = network.input_container.labels_list[network.input_container.train_labels[k].argmax()]
  ent_dict[cur_label].add(e1)
  ent_dict[cur_label].add(e2)
  k+=1

for key in ent_dict_kbp37_superv_test:
  print key + str(len(ent_dict_kbp37_superv_test[key]))
  print len(ent_dict_kbp37_superv_test[key] & ent_dict_kbp37_superv_train[key])