import re
from bs4 import BeautifulSoup
import os

for split in os.listdir('splits'):
  abstr_list = open(os.path.join('splits', split), 'r')
  with open(split + '.out', 'w') as outp:
    for abstr_name in abstr_list.readlines():
      f = open(os.path.join('aimed_abstracts', abstr_name[:-1]))
      for line in f.readlines():
        if line.count('<prot>') >= 2:
          line = re.sub('\n', '', line)
          soap = BeautifulSoup(line, 'html.parser')
          # print "-------------------------"
          print line
          # NB: changing direction is wrong here! does not affect because we do not have examples with different direction
          if line.count('<p1  pair=') > 0:
            e2 = ""
            for pair_part in soap.find_all('p1'):
              pair_number = pair_part.get('pair')
              e1 = soap.find('p1', {'pair':pair_number}).get_text()
              e2_tag = soap.find('p2', {'pair':pair_number})
              if e2_tag == None:
                continue
              else:
                e2 = e2_tag.get_text()
                example = re.sub('<p1  pair='+str(pair_number), '<e1>'+e1+'</e1><p1  pair='+str(pair_number), line)
                example = re.sub('<p2  pair='+str(pair_number), '<e2>'+e2+'</e2><p2  pair='+str(pair_number), example)
                example = re.sub('<p1  pair='+str(pair_number)+'[^/]*/prot>', '', example)
                example = re.sub('<p1[^>]*>', '', example)
                example = re.sub('<p2  pair='+str(pair_number)+'[^/]*/prot>', '', example)
                example = re.sub('<p2[^>]*>', '', example)
                example = re.sub('</p1>', '', example)
                example = re.sub('</p2>', '', example)
                example = re.sub('<prot>', '', example)
                example = re.sub('</prot>', '', example)
                # print example
                outp.write('"' + example + '"\n')
                if example.find('<e1>') > example.find('<e2>'):
                  outp.write('interaction(e2,e1)\n\n')
                else:
                  outp.write('interaction(e1,e2)\n\n')
            for pair_part1 in soap.find_all('p1'):
              for pair_part2 in pair_part1.find_all_next('p1'):
                e1 = pair_part1.get_text()
                pair_number1 = pair_part1.get('pair')
                e2 = pair_part2.get_text()
                pair_number2 = pair_part2.get('pair')
                if e1.replace(" ", "") == e2.replace(" ", ""):
                  continue
                else:
                  example = re.sub('<p1  pair='+str(pair_number1), '<e1>'+e1+'</e1><p1  pair='+str(pair_number1), line)
                  example = re.sub('<p1  pair='+str(pair_number2), '<e2>'+e2+'</e2><p1  pair='+str(pair_number2), example)
                  example = re.sub('<p1  pair='+str(pair_number1)+'[^/]*/prot>', '', example)
                  example = re.sub('<p1  pair='+str(pair_number2)+'[^/]*/prot>', '', example)
                  example = re.sub('<p1[^>]*>', '', example)
                  example = re.sub('<p2[^>]*>', '', example)
                  example = re.sub('</p1>', '', example)
                  example = re.sub('</p2>', '', example)
                  example = re.sub('<prot>', '', example)
                  example = re.sub('</prot>', '', example)
                  # print example
                  outp.write('"' + example + '"\n')
                  outp.write('Other\n\n')
            for pair_part1 in soap.find_all('p2'):
              for pair_part2 in pair_part1.find_all_next('p2'):
                e1 = pair_part1.get_text()
                pair_number1 = pair_part1.get('pair')
                e2 = pair_part2.get_text()
                pair_number2 = pair_part2.get('pair')
                if e1.replace(" ", "") == e2.replace(" ", ""):
                  continue
                else:
                  example = re.sub('<p2  pair='+str(pair_number1), '<e1>'+e1+'</e1><p2  pair='+str(pair_number1), line)
                  example = re.sub('<p2  pair='+str(pair_number2), '<e2>'+e2+'</e2><p2  pair='+str(pair_number2), example)
                  example = re.sub('<p2  pair='+str(pair_number1)+'[^/]*/prot>', '', example)
                  example = re.sub('<p2  pair='+str(pair_number2)+'[^/]*/prot>', '', example)
                  example = re.sub('<p1[^>]*>', '', example)
                  example = re.sub('<p2[^>]*>', '', example)
                  example = re.sub('</p1>', '', example)
                  example = re.sub('</p2>', '', example)
                  example = re.sub('<prot>', '', example)
                  example = re.sub('</prot>', '', example)
                  # print example
                  outp.write('"' + example + '"\n')
                  outp.write('Other\n\n')
          else:
            for pair_part1 in soap.find_all('prot'):
              for pair_part2 in pair_part1.find_all_next('prot'):
                if pair_part1 is pair_part2:
                  continue
                else:
                  e1 = pair_part1.get_text()
                  e2 = pair_part2.get_text()
                  pair_part1.wrap(soap.new_tag('e1'))
                  pair_part2.wrap(soap.new_tag('e2'))
                  example = re.sub('<prot>', '', str(soap))
                  example = re.sub('</prot>', '', example)
                  soap.e1.unwrap()
                  soap.e2.unwrap()
                  if example.find('<e1>') >= 0 and example.find('<e2>') >= 0 and (example.find('</e1>') < example.find('<e2>') or example.find('</e2>') < example.find('<e1>')):
                    # print example
                    outp.write('"' + example + '"\n')
                    outp.write('Other\n\n')
      f.close()



with open('aimed_dataset.txt', 'w') as outp:
  for abstr in os.listdir('aimed_abstracts'):
    f = open(os.path.join('aimed_abstracts', abstr))
    for line in f.readlines():
      if line.count('<prot>') >= 2:
        line = re.sub('\n', '', line)
        soap = BeautifulSoup(line, 'html.parser')
        # print "-------------------------"
        print line
        # NB: changing direction is wrong here! does not affect because we do not have examples with different direction
        if line.count('<p1  pair=') > 0:
          e2 = ""
          for pair_part in soap.find_all('p1'):
            pair_number = pair_part.get('pair')
            e1 = soap.find('p1', {'pair':pair_number}).get_text()
            e2_tag = soap.find('p2', {'pair':pair_number})
            if e2_tag == None:
              continue
            else:
              e2 = e2_tag.get_text()
              example = re.sub('<p1  pair='+str(pair_number), '<e1>'+e1+'</e1><p1  pair='+str(pair_number), line)
              example = re.sub('<p2  pair='+str(pair_number), '<e2>'+e2+'</e2><p2  pair='+str(pair_number), example)
              example = re.sub('<p1  pair='+str(pair_number)+'[^/]*/prot>', '', example)
              example = re.sub('<p1[^>]*>', '', example)
              example = re.sub('<p2  pair='+str(pair_number)+'[^/]*/prot>', '', example)
              example = re.sub('<p2[^>]*>', '', example)
              example = re.sub('</p1>', '', example)
              example = re.sub('</p2>', '', example)
              example = re.sub('<prot>', '', example)
              example = re.sub('</prot>', '', example)
              # print example
              outp.write('"' + example + '"\n')
              if example.find('<e1>') > example.find('<e2>'):
                outp.write('interaction(e2,e1)\n\n')
              else:
                outp.write('interaction(e1,e2)\n\n')
          for pair_part1 in soap.find_all('p1'):
            for pair_part2 in pair_part1.find_all_next('p1'):
              e1 = pair_part1.get_text()
              pair_number1 = pair_part1.get('pair')
              e2 = pair_part2.get_text()
              pair_number2 = pair_part2.get('pair')
              if e1.replace(" ", "") == e2.replace(" ", ""):
                continue
              else:
                example = re.sub('<p1  pair='+str(pair_number1), '<e1>'+e1+'</e1><p1  pair='+str(pair_number1), line)
                example = re.sub('<p1  pair='+str(pair_number2), '<e2>'+e2+'</e2><p1  pair='+str(pair_number2), example)
                example = re.sub('<p1  pair='+str(pair_number1)+'[^/]*/prot>', '', example)
                example = re.sub('<p1  pair='+str(pair_number2)+'[^/]*/prot>', '', example)
                example = re.sub('<p1[^>]*>', '', example)
                example = re.sub('<p2[^>]*>', '', example)
                example = re.sub('</p1>', '', example)
                example = re.sub('</p2>', '', example)
                example = re.sub('<prot>', '', example)
                example = re.sub('</prot>', '', example)
                # print example
                outp.write('"' + example + '"\n')
                outp.write('Other\n\n')
          for pair_part1 in soap.find_all('p2'):
            for pair_part2 in pair_part1.find_all_next('p2'):
              e1 = pair_part1.get_text()
              pair_number1 = pair_part1.get('pair')
              e2 = pair_part2.get_text()
              pair_number2 = pair_part2.get('pair')
              if e1.replace(" ", "") == e2.replace(" ", ""):
                continue
              else:
                example = re.sub('<p2  pair='+str(pair_number1), '<e1>'+e1+'</e1><p2  pair='+str(pair_number1), line)
                example = re.sub('<p2  pair='+str(pair_number2), '<e2>'+e2+'</e2><p2  pair='+str(pair_number2), example)
                example = re.sub('<p2  pair='+str(pair_number1)+'[^/]*/prot>', '', example)
                example = re.sub('<p2  pair='+str(pair_number2)+'[^/]*/prot>', '', example)
                example = re.sub('<p1[^>]*>', '', example)
                example = re.sub('<p2[^>]*>', '', example)
                example = re.sub('</p1>', '', example)
                example = re.sub('</p2>', '', example)
                example = re.sub('<prot>', '', example)
                example = re.sub('</prot>', '', example)
                # print example
                outp.write('"' + example + '"\n')
                outp.write('Other\n\n')
        else:
          for pair_part1 in soap.find_all('prot'):
            for pair_part2 in pair_part1.find_all_next('prot'):
              if pair_part1 is pair_part2:
                continue
              else:
                e1 = pair_part1.get_text()
                e2 = pair_part2.get_text()
                pair_part1.wrap(soap.new_tag('e1'))
                pair_part2.wrap(soap.new_tag('e2'))
                example = re.sub('<prot>', '', str(soap))
                example = re.sub('</prot>', '', example)
                soap.e1.unwrap()
                soap.e2.unwrap()
                if example.find('<e1>') >= 0 and example.find('<e2>') >= 0 and (example.find('</e1>') < example.find('<e2>') or example.find('</e2>') < example.find('<e1>')):
                  # print example
                  outp.write('"' + example + '"\n')
                  outp.write('Other\n\n')

    f.close()
