import re

data = open("RosarioHerst, treat-prevent, sentences_with_roles_and_relations.txt","r")
res = []

for line in data.readlines():
  class_name = line.split("||")[1][:-1]
  # print class_name
  sent = line.split("||")[0]
  if class_name == 'TREAT_FOR_DIS':
    e1 = '<TREAT>'
    e1_cl = '</TREAT>'
    e2 = '<DIS>'
    e2_cl = '</DIS>'
    label = 'treatment_for'
  if class_name == 'PREVENT':
    e1 = '<TREAT_PREV>'
    e1_cl = '</TREAT_PREV>'
    e2 = '<DIS_PREV>'
    e2_cl = '</DIS_PREV>'
    label = 'prevents_from'
  if class_name == 'VAGUE':
    e1 = '<TREAT_VAG>'
    e1_cl = '</TREAT_VAG>'
    e2 = '<DIS_VAG>'
    e2_cl = '</DIS_VAG>'
    label = 'Other'
  if class_name == 'SIDE_EFF':
    e1 = '<TREAT_SIDE_EFF>'
    e1_cl = '</TREAT_SIDE_EFF>'
    e2 = '<DIS_SIDE_EFF>'
    e2_cl = '</DIS_SIDE_EFF>'
    label = 'Other'
  if class_name == 'TREAT_NO_FOR_DIS':
    e1 = '<TREAT_NO>'
    e1_cl = '</TREAT_NO>'
    e2 = '<DIS_NO>'
    e2_cl = '</DIS_NO>'
    label = 'Other'
  if class_name == 'NONE' or class_name == 'DISONLY' or class_name == 'TREATONLY' or class_name == 'TO_SEE':
    continue

  p = re.compile(e1)
  sent = re.sub(p, '<e1>', sent, count=1)
  sent = re.sub(p, '', sent)
  p = re.compile(e1_cl)
  sent = re.sub(p, '</e1>', sent, count=1)
  sent = re.sub(p, '', sent)
  p = re.compile(e2)
  sent = re.sub(p, '<e2>', sent, count=1)
  sent = re.sub(p, '', sent)
  p = re.compile(e2_cl)
  sent = re.sub(p, '</e2>', sent, count=1)
  sent = re.sub(p, '', sent)

  res.append('"' + sent + '"')
  if label == 'Other':
    res.append(label)
  else:
    if sent.find('<e1>') > sent.find('<e2>'):
      sent = sent.replace('<e1>', '<e3>')
      sent = sent.replace('</e1>', '</e3>')
      sent = sent.replace('<e2>', '<e1>')
      sent = sent.replace('</e2>', '</e1>')
      sent = sent.replace('<e3>', '<e2>')
      sent = sent.replace('</e3>', '</e2>')
      res.append(label + '(e2,e1)')
    else:
      res.append(label + '(e1,e2)')
  res.append('')


with open('cleaned_dataset.txt', 'w') as outp:
  for line in res:
    outp.write(line + '\n')
