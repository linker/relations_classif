Counter({'Other': 37,
         'has_side_effect(e1,e2)': 12,
         'has_side_effect(e2,e1)': 18,
         'not_a_treatment_for(e1,e2)': 4,
         'prevents_from(e1,e2)': 44,
         'prevents_from(e2,e1)': 19,
         'treatment_for(e1,e2)': 640,
         'treatment_for(e2,e1)': 190})

test_data = []
train_data = []
train_counts = {}
test_counts = {}

for i in range(len(classes)):
    cur_class = classes[i]
    if cur_class=='Other':
        border = 800
        test_border = 300
    if cur_class=='interaction(e1,e2)':
        border = 780
        test_border = 265
    if cur_class=='interaction(e2,e1)':
        border = 5
        test_border = 5
    if cur_class=='has_side_effect(e1,e2)':
        border = 9
    if cur_class=='has_side_effect(e2,e1)':
        border = 12
    if cur_class=='not_a_treatment_for(e1,e2)':
        border = 3
    if cur_class=='prevents_from(e1,e2)':
        border = 33
    if cur_class=='prevents_from(e2,e1)':
        border = 15
    if cur_class=='treatment_for(e1,e2)':
        border = 480
    if cur_class=='treatment_for(e2,e1)':
        border = 140

    if train_counts.get(cur_class) == None:
        train_counts[cur_class] = 1
        train_data.append(quot_sents[i])
        train_data.append(cur_class)
        train_data.append('')
    else:
        if train_counts[cur_class] >= border:
            if test_counts.get(cur_class) == None:
                test_counts[cur_class] = 1
                test_data.append(quot_sents[i])
                test_data.append(cur_class)
                test_data.append('')
            else:
                if test_counts[cur_class] < test_border:
                    test_counts[cur_class] += 1
                    test_data.append(quot_sents[i])
                    test_data.append(cur_class)
                    test_data.append('')
        else:
            train_counts[cur_class] += 1
            train_data.append(quot_sents[i])
            train_data.append(cur_class)
            train_data.append('')
