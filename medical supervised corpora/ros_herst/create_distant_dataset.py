f = open("tags.txt","r")
tags = {}

for line in f.readlines():
  if len(line.split('|')) < 9:
    continue
  if line.split('|')[1]=='MMI':
#     sem_type = line.split('|')[5][1:-1]
#     if 'clnd' in sem_type or 'phsu' in sem_type or 'antb' in sem_type or 'bacs' in sem_type or 'chvs' in sem_type or 'aapp' in sem_type:
    tags[line.split('|')[4]] = line.split('|')[8]

kb = open("treated_kb_entity_pairs.txt","r")
pairs_ids = []
for line in kb.readlines():
  pairs_ids.append(line.split('\t')[0] + '|' + line.split('\t')[1])

text = open("input.txt", "r").read()


from pprint import pprint
import re
pprint('Indexing sentences...')

indexes = [0] + [m.start() for m in re.finditer('\n', text)]
# pprint(indexes)

ranges = zip(indexes[:-1], indexes[1:])
# pprint(ranges)

def first_range(x, ranges=ranges):
    for i, r in enumerate(ranges):
        if r[0] <= x <= r[1]:
            return i
    return -1

pprint('Getting sences for pairs...')
sentence_indexes = {}
for id, locs in tags.iteritems():
    for loc in locs.strip().split(';'):
        # several triggers for term
        if loc[0] == '[':
            loc = loc.split('[')[1].split(']')[0]
        # several terms in one trigger
        # here we do not care about the next words - they anyway will be in the same sentence
        if len(loc.split(',')) > 1:
            loc = loc.split(',')[0]
        sentence_indexes.setdefault(id, set()).add(first_range(int(loc.split('/')[0])))
# pprint(sentence_indexes)

pairs_sentences = {}
for pair in pairs_ids:
    first_id, second_id = pair.strip().split('|')
    intersection = set.intersection(sentence_indexes.get(first_id, set()), sentence_indexes.get(second_id, set()))
    if len(intersection) != 0:
        pairs_sentences[pair] = intersection

# pprint(pairs_sentences)


pprint('Writing down dataset...')
# order in KB
# position one is medicine
# position two is desease
outp = open("distant_dataset.txt","w")
for pair in pairs_sentences:
    pprint('---------')
    e1_id = pair.split('|')[0]
    e2_id = pair.split('|')[1]
    for sent_id in pairs_sentences[pair]:
        sent_range = ranges[sent_id]
        sent = text[sent_range[0]:sent_range[1]]
        pprint(sent)

        for e1_position in tags[e1_id].split(';'):
            if e1_position[0] == '[':
                e1_position = e1_position.split('[')[1].split(']')[0]
            e1_start = int(e1_position.split(',')[0].split('/')[0])
            e1_end = -1
            if e1_start >= sent_range[0] and e1_start <= sent_range[1]:
                e1_start -= sent_range[0]
                e1_end = int(e1_position.split(',')[-1].split('/')[0]) + int(e1_position.split(',')[-1].split('/')[1]) - sent_range[0]
                break
        if e1_end == -1:
            print 'Smth went wrong'
        else:
            pprint(sent[e1_start:e1_end])

        for e2_position in tags[e2_id].split(';'):
            if e2_position[0] == '[':
                e2_position = e2_position.split('[')[1].split(']')[0]
            e2_start = int(e2_position.split(',')[0].split('/')[0])
            e2_end = -1
            if e2_start >= sent_range[0] and e2_start <= sent_range[1]:
                e2_start -= sent_range[0]
                e2_end = int(e2_position.split(',')[-1].split('/')[0]) + int(e2_position.split(',')[-1].split('/')[1]) - sent_range[0]
                break
        if e2_end == -1:
            print 'Smth went wrong'
        else:
            pprint(sent[e2_start:e2_end])

        shift = 0
        if e1_start < e2_start:
            sent = sent[:e1_start] + '<e1>' + sent[e1_start:]
            shift += len('<e1>')
            sent = sent[:e1_end + shift] + '</e1>' + sent[e1_end + shift:]
            shift += len('</e1>')
            sent = sent[:e2_start + shift] + '<e2>' + sent[e2_start + shift:]
            shift += len('<e2>')
            sent = sent[:e2_end + shift] + '</e2>' + sent[e2_end + shift:]
            sent = sent[1:]
            # pprint(sent)
            # pprint('may_be_treated_by(e1,e2)')
            outp.write('"' + sent + '"\ntreatment_for(e1,e2)\n\n')
        else:
            sent = sent[:e2_start] + '<e1>' + sent[e2_start:]
            shift += len('<e1>')
            sent = sent[:e2_end + shift] + '</e1>' + sent[e2_end + shift:]
            shift += len('</e1>')
            sent = sent[:e1_start + shift] + '<e2>' + sent[e1_start + shift:]
            shift += len('<e2>')
            sent = sent[:e1_end + shift] + '</e2>' + sent[e1_end + shift:]
            sent = sent[1:]
            # pprint(sent)
            # pprint('may_be_treated_by(e2,e1)')
            outp.write('"' + sent + '"\ntreatment_for(e2,e1)\n\n')

pprint('Writing down Other class dataset...')
other_pairs_sentences = {}
for e1_id in sentence_indexes:
    if len(other_pairs_sentences) > 500:
        break
    e1_id_counter = 0
    for e2_id in sentence_indexes:
        if e1_id_counter > 2:
            break
        if e1_id == e2_id:
            continue
        else:
            if e1_id + '|' + e2_id in pairs_ids or e2_id + '|' + e1_id in pairs_ids or e1_id + '|' + e2_id in other_pairs_sentences.keys():
                continue
            else:
                intersection = set.intersection(sentence_indexes[e1_id], sentence_indexes[e2_id])
                if len(intersection) > 0:
                    other_pairs_sentences[e1_id + '|' + e2_id] = intersection
                    e1_id_counter += 1

# pprint(other_pairs_sentences)


for pair in other_pairs_sentences:
    pprint('---------')
    e1_id = pair.split('|')[0]
    e2_id = pair.split('|')[1]
    for sent_id in other_pairs_sentences[pair]:
        sent_range = ranges[sent_id]
        sent = text[sent_range[0]:sent_range[1]]
        pprint(sent)

        for e1_position in tags[e1_id].split(';'):
            if e1_position[0] == '[':
                e1_position = e1_position.split('[')[1].split(']')[0]
            e1_start = int(e1_position.split(',')[0].split('/')[0])
            e1_end = -1
            if e1_start >= sent_range[0] and e1_start <= sent_range[1]:
                e1_start -= sent_range[0]
                e1_end = int(e1_position.split(',')[-1].split('/')[0]) + int(e1_position.split(',')[-1].split('/')[1]) - sent_range[0]
                break
        if e1_end == -1:
            print 'Smth went wrong'
        else:
            pprint(sent[e1_start:e1_end])

        for e2_position in tags[e2_id].split(';'):
            if e2_position[0] == '[':
                e2_position = e2_position.split('[')[1].split(']')[0]
            e2_start = int(e2_position.split(',')[0].split('/')[0])
            e2_end = -1
            if e2_start >= sent_range[0] and e2_start <= sent_range[1]:
                e2_start -= sent_range[0]
                e2_end = int(e2_position.split(',')[-1].split('/')[0]) + int(e2_position.split(',')[-1].split('/')[1]) - sent_range[0]
                break
        if e2_end == -1:
            print 'Smth went wrong'
        else:
            pprint(sent[e2_start:e2_end])

        shift = 0
        if e1_start < e2_start:
            sent = sent[:e1_start] + '<e1>' + sent[e1_start:]
            shift += len('<e1>')
            sent = sent[:e1_end + shift] + '</e1>' + sent[e1_end + shift:]
            shift += len('</e1>')
            sent = sent[:e2_start + shift] + '<e2>' + sent[e2_start + shift:]
            shift += len('<e2>')
            sent = sent[:e2_end + shift] + '</e2>' + sent[e2_end + shift:]
            sent = sent[1:]
            # pprint(sent)
            # pprint('Other')
            outp.write('"' + sent + '"\nOther\n\n')
        else:
            sent = sent[:e2_start] + '<e1>' + sent[e2_start:]
            shift += len('<e1>')
            sent = sent[:e2_end + shift] + '</e1>' + sent[e2_end + shift:]
            shift += len('</e1>')
            sent = sent[:e1_start + shift] + '<e2>' + sent[e1_start + shift:]
            shift += len('<e2>')
            sent = sent[:e1_end + shift] + '</e2>' + sent[e1_end + shift:]
            sent = sent[1:]
            # pprint(sent)
            # pprint('Other')
            outp.write('"' + sent + '"\nOther\n\n')

