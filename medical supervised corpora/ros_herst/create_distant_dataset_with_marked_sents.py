f = open("tags.txt","r")
tags = {}

for line in f.readlines():
  if len(line.split('|')) < 9:
    continue
  if line.split('|')[1]=='MMI':
#     sem_type = line.split('|')[5][1:-1]
#     if 'clnd' in sem_type or 'phsu' in sem_type or 'antb' in sem_type or 'bacs' in sem_type or 'chvs' in sem_type or 'aapp' in sem_type:
    tags.setdefault(line.split('|')[4], {}).update({line.split('|')[0]: line.split('|')[8]})
f.close()

kb = open("treated_kb_entity_pairs.txt","r")
pairs_ids = []
for line in kb.readlines():
  pairs_ids.append(line.split('\t')[0] + '|' + line.split('\t')[1])
kb.close()

#text = open("input.txt", "r").read()
f = open("input.txt", "r")
sents = [] 

i = 0
prev_line = ''
for line in f.readlines():
  if i%3 == 0:
    prev_line = line
    i += 1
    continue
  if i%3 == 1:
    sents.append(prev_line + line)
  i += 1
f.close()

from pprint import pprint
import re

pprint('Getting sences for pairs...')
f = open("tags.txt","r")
sentence_indexes = {}
for line in f.readlines():
  if len(line.split('|')) < 9:
    continue
  if line.split('|')[1]=='MMI':
    sentence_indexes.setdefault(line.split('|')[4], set()).add(line.split('|')[0])
f.close()

pairs_sentences = {}
for pair in pairs_ids:
    first_id, second_id = pair.strip().split('|')
    intersection = set.intersection(sentence_indexes.get(first_id, set()), sentence_indexes.get(second_id, set()))
    if len(intersection) != 0:
        pairs_sentences[pair] = intersection

# pprint(pairs_sentences)


pprint('Writing down dataset...')
# order in KB
# position one is medicine
# position two is desease
outp = open("distant_dataset.txt","w")
for pair in pairs_sentences:
    pprint('---------')
    e1_id = pair.split('|')[0]
    e2_id = pair.split('|')[1]
    for sent_id in pairs_sentences[pair]:
        sent = sents[int(sent_id)]
        pprint(sent)

        for e1_position in tags[e1_id][sent_id].split(';'):
            if e1_position[0] == '[':
                e1_position = e1_position.split('[')[1].split(']')[0]
            e1_start = int(e1_position.split(',')[0].split('/')[0])
            e1_end = int(e1_position.split(',')[-1].split('/')[0]) + int(e1_position.split(',')[-1].split('/')[1])
            break
        pprint(sent[e1_start:e1_end])

        for e2_position in tags[e2_id][sent_id].split(';'):
            if e2_position[0] == '[':
                e2_position = e2_position.split('[')[1].split(']')[0]
            e2_start = int(e2_position.split(',')[0].split('/')[0])
            e2_end = int(e2_position.split(',')[-1].split('/')[0]) + int(e2_position.split(',')[-1].split('/')[1])
            break
        pprint(sent[e2_start:e2_end])

        if e1_start == e2_start or e1_end == e2_end:
          continue

        shift = 0
        if e1_start < e2_start:
            sent = sent[:e1_start] + '<e1>' + sent[e1_start:]
            shift += len('<e1>')
            sent = sent[:e1_end + shift] + '</e1>' + sent[e1_end + shift:]
            shift += len('</e1>')
            sent = sent[:e2_start + shift] + '<e2>' + sent[e2_start + shift:]
            shift += len('<e2>')
            sent = sent[:e2_end + shift] + '</e2>' + sent[e2_end + shift:]
            sent = sent[1:]
            # pprint(sent.split('\n')[1][6:])
            # pprint('may_be_treated_by(e1,e2)')
            outp.write('"' + sent.split('\n')[1][6:] + '"\ntreatment_for(e1,e2)\n\n')
        else:
            sent = sent[:e2_start] + '<e1>' + sent[e2_start:]
            shift += len('<e1>')
            sent = sent[:e2_end + shift] + '</e1>' + sent[e2_end + shift:]
            shift += len('</e1>')
            sent = sent[:e1_start + shift] + '<e2>' + sent[e1_start + shift:]
            shift += len('<e2>')
            sent = sent[:e1_end + shift] + '</e2>' + sent[e1_end + shift:]
            sent = sent[1:]
            # pprint(sent.split('\n')[1][6:])
            # pprint('may_be_treated_by(e2,e1)')
            outp.write('"' + sent.split('\n')[1][6:] + '"\ntreatment_for(e2,e1)\n\n')

pprint('Writing down Other class dataset...')
other_pairs_sentences = {}
for e1_id in sentence_indexes:
    if len(other_pairs_sentences) > 500:
        break
    e1_id_counter = 0
    for e2_id in sentence_indexes:
        if e1_id_counter > 2:
            break
        if e1_id == e2_id:
            continue
        else:
            if e1_id + '|' + e2_id in pairs_ids or e2_id + '|' + e1_id in pairs_ids or e1_id + '|' + e2_id in other_pairs_sentences.keys():
                continue
            else:
                intersection = set.intersection(sentence_indexes[e1_id], sentence_indexes[e2_id])
                if len(intersection) > 0:
                    other_pairs_sentences[e1_id + '|' + e2_id] = intersection
                    e1_id_counter += 1

# pprint(other_pairs_sentences)


for pair in other_pairs_sentences:
    pprint('---------')
    e1_id = pair.split('|')[0]
    e2_id = pair.split('|')[1]
    for sent_id in other_pairs_sentences[pair]:
        sent = sents[int(sent_id)]
        pprint(sent)

        for e1_position in tags[e1_id][sent_id].split(';'):
            if e1_position[0] == '[':
                e1_position = e1_position.split('[')[1].split(']')[0]
            e1_start = int(e1_position.split(',')[0].split('/')[0])
            e1_end = int(e1_position.split(',')[-1].split('/')[0]) + int(e1_position.split(',')[-1].split('/')[1])
            break
        pprint(sent[e1_start:e1_end])

        for e2_position in tags[e2_id][sent_id].split(';'):
            if e2_position[0] == '[':
                e2_position = e2_position.split('[')[1].split(']')[0]
            e2_start = int(e2_position.split(',')[0].split('/')[0])
            e2_end = int(e2_position.split(',')[-1].split('/')[0]) + int(e2_position.split(',')[-1].split('/')[1])
            break
        pprint(sent[e2_start:e2_end])

        if e1_start == e2_start or e1_end == e2_end:
          continue

        shift = 0
        if e1_start < e2_start:
            sent = sent[:e1_start] + '<e1>' + sent[e1_start:]
            shift += len('<e1>')
            sent = sent[:e1_end + shift] + '</e1>' + sent[e1_end + shift:]
            shift += len('</e1>')
            sent = sent[:e2_start + shift] + '<e2>' + sent[e2_start + shift:]
            shift += len('<e2>')
            sent = sent[:e2_end + shift] + '</e2>' + sent[e2_end + shift:]
            sent = sent[1:]
            #pprint(sent.split('\n')[1][6:])
            #pprint('Other')
            outp.write('"' + sent.split('\n')[1][6:] + '"\nOther\n\n')
        else:
            sent = sent[:e2_start] + '<e1>' + sent[e2_start:]
            shift += len('<e1>')
            sent = sent[:e2_end + shift] + '</e1>' + sent[e2_end + shift:]
            shift += len('</e1>')
            sent = sent[:e1_start + shift] + '<e2>' + sent[e1_start + shift:]
            shift += len('<e2>')
            sent = sent[:e1_end + shift] + '</e2>' + sent[e1_end + shift:]
            sent = sent[1:]
            #pprint(sent.split('\n')[1][6:])
            #pprint('Other')
            outp.write('"' + sent.split('\n')[1][6:] + '"\nOther\n\n')

