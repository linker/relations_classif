from bs4 import BeautifulSoup
import os

f = open('gold annotations test.txt', 'r')
gold_annot_pos = []
for line in f.readlines():
  if line.split('\t')[2][:-1]=='1':
    gold_annot_pos.append(line.split('\t')[0] + '___' + line.split('\t')[1])
f.close()

with open('ddi_test.txt', 'w') as outp:
  for descr in os.listdir('test'):
    descr_text = open(os.path.join('test', descr), 'r').read()
    soup = BeautifulSoup(descr_text)
    for sent in soup.find_all('sentence'):
      # print "-------------------"
      print sent.prettify()
      # NB: changing direction is wrong here! does not affect because we do not have examples with different direction
      for pair in sent.find_all('pair'):
        e1_id = pair.get('e1')
        e2_id = pair.get('e2')
        e1 = sent.find('entity', id=e1_id)
        e2 = sent.find('entity', id=e2_id)
        example = sent.get('text')
        e1_offset = e1.get('charoffset').split('-')
        example = example[0:int(e1_offset[0])] + '<e1>' + example[int(e1_offset[0]):int(e1_offset[1])] + '</e1>' + example[int(e1_offset[1]):len(example)]
        e2_offset = e2.get('charoffset').split('-')
        if int(e1_offset[0]) < int(e2_offset[0]):
          example = example[0:int(e2_offset[0])+9] + '<e2>' + example[int(e2_offset[0])+9:int(e2_offset[1])+9] + '</e2>' + example[int(e2_offset[1])+9:len(example)+9]
        else:
          example = example[0:int(e2_offset[0])] + '<e2>' + example[int(e2_offset[0]):int(e2_offset[1])] + '</e2>' + example[int(e2_offset[1]):len(example)]
        # print example
        outp.write('"' + example + '"\n')
        if example.find('<e1>') >= 0 and example.find('<e2>') >= 0:
          if str(e1_id+'___'+e2_id) in gold_annot_pos:
            # print 'interaction(e1,e2)'
            outp.write('interaction(e1,e2)\n\n')
          else:
            if str(e2_id+'___'+e1_id) in gold_annot_pos:
              # print 'interaction(e2,e1)'
              outp.write('interaction(e2,e1)\n\n')
            else:
              # print 'Other'
              outp.write('Other\n\n')


with open('ddi_dataset.txt', 'w') as outp:
  for descr in os.listdir('drug_descripts'):
    descr_text = open(os.path.join('drug_descripts', descr), 'r').read()
    soup = BeautifulSoup(descr_text)
    for sent in soup.find_all('sentence'):
      # print "-------------------"
      print sent.prettify()
      # NB: changing direction is wrong here! does not affect because we do not have examples with different direction
      for pair in sent.find_all('pair'):
        e1_id = pair.get('e1')
        e2_id = pair.get('e2')
        e1 = sent.find('entity', id=e1_id)
        e2 = sent.find('entity', id=e2_id)
        example = sent.get('text')
        e1_offset = e1.get('charoffset').split('-')
        example = example[0:int(e1_offset[0])] + '<e1>' + example[int(e1_offset[0]):int(e1_offset[1])] + '</e1>' + example[int(e1_offset[1]):len(example)]
        e2_offset = e2.get('charoffset').split('-')
        if int(e1_offset[0]) < int(e2_offset[0]):
          example = example[0:int(e2_offset[0])+9] + '<e2>' + example[int(e2_offset[0])+9:int(e2_offset[1])+9] + '</e2>' + example[int(e2_offset[1])+9:len(example)+9]
        else:
          example = example[0:int(e2_offset[0])] + '<e2>' + example[int(e2_offset[0]):int(e2_offset[1])] + '</e2>' + example[int(e2_offset[1]):len(example)]
        # print example
        outp.write('"' + example + '"\n')
        if example.find('<e1>') >= 0 and example.find('<e2>') >= 0:
          if pair.get('interaction') == 'true':
            if example.find('<e1>') > example.find('<e2>'):
              # print 'interaction(e2,e1)'
              outp.write('interaction(e2,e1)\n\n')
            else:
              # print 'interaction(e1,e2)'
              outp.write('interaction(e1,e2)\n\n')
          else:
            # print 'Other'
            outp.write('Other\n\n')

