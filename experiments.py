from relations_model.relations_cnn import RelationsCnn

network = RelationsCnn()

### load embeddings
### emb_length is to choose what is the length, for glove there are 50, 100, 200 and 300, for word2vec 400
### load_full_vocab will create embeddings matric from word_index of embeddings, not of the train and test data
### it is slow, but more correct
### load data and transform it to sequences and 1-hot labels
network.prepare_input(emb_name='word2vec', emb_length=300, load_full_vocab=False, dataset_name='semeval', zero_position=100)

### initialize model
network.init_model(distances_emb_length=30, train_words_emb=False, train_distance_emb=True, filters_num=1000, window_size=3, regulize_rate=0.001)
### or load model from previous experiments
# network.load_model(model_file, weights_file)
### train for 15 epochs starting from 0; critical because we have learning rate decay
### as a result will be saved several files - losses_timestamp.txt, avg_losses_timestamp.txt, accuracies_timestamp.txt, weights_timestamp.txt
### False for no printing info except for progress bar
network.train(15, start_epoch=0, verbose=False)
### can save model (layers structure) to file
network.save_model("model_from_experiment.txt")

### create validation prediction files for current model and dataset
network.form_evaluation_files()


### like in the paper
network.prepare_input(emb_name='word2vec', emb_length=400, load_full_vocab=False, dataset_name='semeval', zero_position=100)
network.init_model(distances_emb_length=70, train_words_emb=True, train_distance_emb=True, filters_num=1000, window_size=3, regulize_rate=0.001)
network.train(20, start_epoch=0, verbose=False, check_train_accuracy=False)


### my simple experiments
network.prepare_input(emb_name='glove', emb_length=300, load_full_vocab=False, dataset_name='semeval', zero_position=100)
network.init_model(distances_emb_length=40, train_words_emb=False, train_distance_emb=True, filters_num=1000, window_size=3, regulize_rate=0.001)
