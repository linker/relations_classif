from relations_model.arbitrary_read import ArbitraryRead

zero_position = 400
reader = ArbitraryRead("kbp37_distant_ds", zero_position)
test_data, test_labels = reader.get_test_data()
train_data, train_labels = reader.get_train_data()
print reader.longest_sentence

from collections import Counter
Counter(train_labels)
classes = train_labels
unbal = train_data
border =
train_labels = []
train_data = []
train_counts = {}
for i in range(len(classes)):
  cur_class = classes[i]
  if train_counts.get(cur_class) == None:
    train_counts[cur_class] = 1
    train_data.append(unbal[i])
    train_labels.append(cur_class)
  else:
    if train_counts[cur_class] < border:
      train_counts[cur_class] += 1
      train_data.append(unbal[i])
      train_labels.append(cur_class)

from relations_model.kbp37_read import Kbp37Read

reader_gold = Kbp37Read(zero_position)
train_data_gold, train_labels_gold = reader_gold.get_train_data()
print reader_gold.longest_sentence

import numpy as np

train_labels = np.append(train_labels, train_labels_gold)
train_data = np.append(np.asarray(train_data), np.asarray(train_data_gold), axis=0)

from relations_model.relations_cnn import RelationsCnn
from relations_model.input_container import InputContainer

input_container = InputContainer()
input_container.emb_name = "word2vec"
input_container.emb_length = 300
input_container.load_embeddings(load_full_vocab=False)
input_container.prepare_data(train_data, train_labels, test_data, test_labels)

network = RelationsCnn(False)
network.load_input(input_container, zero_position)
network.init_model(distances_emb_length=30, train_words_emb=True, train_distance_emb=True, filters_num=1000, window_size=3, regulize_rate=0.001)
network.train(1, start_epoch=0, verbose=True, check_train_accuracy=True)


from sklearn.metrics import precision_recall_fscore_support
import numpy as np
y_true = []
for label in test_labels:
  if label=='Other':
    y_true.append(0)
  else:
    y_true.append(1)

y_pred = []
i = 0
for seq in network.input_container.test_sequences:
  l = len(seq)
  prediction = network.model.predict([np.asarray(seq).reshape((1,l)), np.asarray(network.input_container.test_d1s[i]).reshape((1,l)), np.asarray(network.input_container.test_d2s[i]).reshape((1,l))]).reshape(network.input_container.n_classes)
  prediction = network.preprocess_prediction(prediction)
  if network.input_container.labels_list[prediction.argmax()]=='Other':
    y_pred.append(0)
  else:
    y_pred.append(1)
  i+=1

print precision_recall_fscore_support(y_true, y_pred, average='binary')
