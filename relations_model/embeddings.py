from glove_emb import GloveEmb
from word2vec_emb import Word2vecEmb
from swivel_emb import SwivelEmb
from character_emb import CharacterEmb

from termcolor import colored

class Embeddings:
  def __init__(self, emb_name, emb_length, load_full_vocab):
    self.emb_name = emb_name
    self.emb_length = emb_length
    self.load_full_vocab = load_full_vocab

  def load(self):
    if self.emb_name == 'word2vec':
      self.load_word2vec_embeddings()
      return

    if self.emb_name == 'swivel':
      self.load_swivel_embeddings()
      return

    if self.emb_name == 'glove':
      self.load_glove_embeddings()
      return

    if self.emb_name == 'char':
      self.load_character_embeddings()
      return

    print colored("Embeddings name should be word2vec, swivel, glove or char!", 'red')

  def load_glove_embeddings(self):
    print colored("Start loading glove embeddings...", 'yellow')
    emb = GloveEmb(self.emb_length)
    self._load_embeddings(emb)

  def load_word2vec_embeddings(self):
    print colored("Start loading word2vec embeddings...", 'yellow')
    emb = Word2vecEmb(self.emb_length)
    self._load_embeddings(emb)

  def load_swivel_embeddings(self):
    print colored("Start loading swivel embeddings...", 'yellow')
    emb = SwivelEmb(self.emb_length)
    self._load_embeddings(emb)

  def load_character_embeddings(self):
    print colored("Start loading character embeddings...", 'yellow')
    emb = CharacterEmb()
    self._load_embeddings(emb)

  def _load_embeddings(self, emb_reader):
    self.embeddings_index = emb_reader.get_emb_index()
    if self.load_full_vocab:
      self.vocab = { word:index+1 for index, word in enumerate(self.embeddings_index) }
      self.vocab_length = len(self.vocab) + 1
      print colored("Embeddings loaded. Words in vocabulary", 'green'), colored(self.vocab_length, 'green')
    else:
      print colored("Embeddings loaded", 'green')

