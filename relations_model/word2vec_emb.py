import numpy as np
from gensim.models.word2vec import *
from gensim.models.keyedvectors import KeyedVectors

class Word2vecEmb:
  def __init__(self, emb_length=400):
    self.emb_length = emb_length
    if emb_length == 400:
      self.model_filename = "wiki_word2vec_emb/wiki_400_modified"
    if emb_length == 300:
      #self.model_filename = "news_word2vec_emb/GoogleNews-vectors-negative300.bin"
      self.model_filename = "pubmed_w2v_300_sven/pubmed_w2v_dim300_iter15_win7_min5_sg1_neg10_sam-3"

  def get_emb_index(self):
    if self.emb_length == 400:
      model = Word2Vec.load(self.model_filename)
    if self.emb_length == 300:
      model = KeyedVectors.load(self.model_filename)  #KeyedVectors.load_word2vec_format(self.model_filename, binary=True)
    embeddings_index = {}
    for word in model.wv.vocab.keys():
        embeddings_index[word] = model[word]

    return embeddings_index


