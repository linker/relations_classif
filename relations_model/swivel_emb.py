import numpy as np

class SwivelEmb:
  def __init__(self, emb_length=400):
    self.emb_length = emb_length
    self.vocab_path = "swivel/vocab_swivel400.txt"
    self.emb_path = "swivel/embeddings_swivel400.bin"

  def get_emb_index(self):
    embeddings_index = {}
    f_emb = open(self.emb_path, "rb")
    embeddings = np.fromfile(f_emb, dtype=np.float32)
    embeddings = embeddings.reshape(embeddings.shape[0]/self.emb_length, self.emb_length)
    f_emb.close()

    f_vocab = open(self.vocab_path)
    i = 0
    for word in f_vocab:
      word = word.split()
      embeddings_index[word[0]] = embeddings[i]
      i += 1
    f_vocab.close()

    return embeddings_index


