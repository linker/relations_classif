from relations_model.semeval_read import SemevalRead
from relations_model.kbp37_read import Kbp37Read

import colorama
from colorama import Fore

class Dataset:
  def __init__(self, dataset_name, zero_position, pad):
    self.dataset_name = dataset_name
    # defines the number that will be the index of the entity, so the longest sentence in the dataset has to be less than zero_position*2+1
    self.zero_position = zero_position
    self.pad = pad

  def load(self):
    if self.dataset_name == 'semeval':
      train_data, train_labels, test_data, test_labels = self.load_semeval_data()
      return train_data, train_labels, test_data, test_labels

    if self.dataset_name == 'kbp37':
      train_data, train_labels, test_data, test_labels = self.load_kbp37_data()
      return train_data, train_labels, test_data, test_labels

    print(Fore.RED + "Dataset name should be semeval or kbp37!")

  def load_semeval_data(self):
    print(Fore.YELLOW + "Start loading train and test data from SemEval...")
    reader = SemevalRead(zero_position=self.zero_position, pad=self.pad)
    train_data, train_labels = reader.get_train_data()
    test_data, test_labels = reader.get_test_data()
    print(Fore.BLUE + "\tThe longest sentence in the dataset is" + str(reader.longest_sentence))
    if reader.longest_sentence + 2 >= self.zero_position:
      print(Fore.RED + "\tWARNING! THE ZERO POSITION IS NOT BIG ENOUGH!")
    print(Fore.GREEN + "Test and train data loaded")
    return train_data, train_labels, test_data, test_labels

  def load_kbp37_data(self):
    print(Fore.YELLOW + "Start loading train and test data from Kbp-37...")
    reader = Kbp37Read(zero_position=self.zero_position, pad=self.pad)
    train_data, train_labels = reader.get_train_data()
    test_data, test_labels = reader.get_test_data()
    print(Fore.BLUE + "\tThe longest sentence in the dataset is" + str(reader.longest_sentence))
    if reader.longest_sentence + 2 >= self.zero_position:
      print(Fore.RED + "\tWARNING! THE ZERO POSITION IS NOT BIG ENOUGH!")
    print(Fore.GREEN + "Test and train data loaded")
    return train_data, train_labels, test_data, test_labels
