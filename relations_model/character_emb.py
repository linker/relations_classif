import numpy as np
import os

class CharacterEmb:
  def __init__(self, emb_length=1100):
    self.emb_path = "character_embeddings_semeval"

  def get_emb_index(self):
    embeddings_index = {}
    f = open(os.path.join(self.emb_path, 'semeval_vocab.txt'))
    emb = np.squeeze(np.load(os.path.join(self.emb_path, 'semeval_before_hw_with_brack.txt.npy')))
    count = 0
    for line in f:
        values = line.split('\t')
        word = values[1][:-1]
        embeddings_index[word] = emb[count]
        count += 1
    f.close()

    return embeddings_index

