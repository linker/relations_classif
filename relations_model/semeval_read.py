import pandas as pd
from nltk import word_tokenize
import numpy as np
import string
import re

class SemevalRead:
  def __init__(self, zero_position=100, pad=False):
    # dataset path
    self.dataset = "SemEval2010_task8_dataset"
    self.longest_sentence = 0
    self.zero_position = zero_position
    self.pad = pad

  # returns two arrays
  # first is of triples - sentence (cleaned and downcased) and two of distances - to e1 and to e2
  # second just of labels
  # size of both is 8000
  def get_train_data(self):
    all_train_data = pd.read_csv(self.dataset + '/TRAIN.txt', sep="\n", header = None)
    return self._get_formatted_examples(all_train_data)

  # all the same, size is 2717
  def get_test_data(self):
    all_test_data = pd.read_csv(self.dataset + '/TEST.txt', sep="\n", header = None)
    return self._get_formatted_examples(all_test_data)

  def _get_formatted_examples(self, read_csv):
    # get sentences (every 3d is a sentence), separate from index in the beginning and transform to numpy array
    data_tagged = read_csv[0][::3].str.split("\t").str.get(1).as_matrix()
    data = []
    for example in data_tagged:
      example = example[1:-1]
      d1, d2, tokenized_example = self._get_distance_arrays(example)
      data.append([tokenized_example, d1, d2])
      if len(tokenized_example) > self.longest_sentence:
        self.longest_sentence = len(tokenized_example)

    # labels are every 3d starting from second line
    labels = read_csv[0][1::3].as_matrix()
    return data, labels

  def _get_distance_arrays(self, example):
    # replace < and > in entities tags with ___, so word_tokenizer will not place them to different tokens
    example = re.sub(r'<(/?e[12])>', r' ___\1___ ', example)
    replacers = ['___e1___', '___e2___', '___/e1___', '___/e2___']
    # find positions of the entitites and indexes array for the tokenized example
    i = 0
    indexes = []
    cleaned_tokenized_example = []
    tokenized_example = list(map(str.lower, word_tokenize(example)))
    tokenized_example = self._padding(tokenized_example)
    for word in tokenized_example:
      if word == '___e1___':
        p1_first = i
      if word == '___e2___':
        p2_first = i
      if word == '___/e1___':
        p1_last = i-1
      if word == '___/e2___':
        p2_last = i-1
      if word not in replacers:
        cleaned_tokenized_example.append(word)
        indexes.append(i)
        i = i+1
    # form distances arrays
    d1 = []
    d2 = []
    for index in indexes:
      if index < p1_first:
        d1.append(index - p1_first + self.zero_position)
      if p1_first <= index and index <= p1_last:
        d1.append(self.zero_position)
      if index > p1_last:
        d1.append(index - p1_last + self.zero_position)

      if index < p2_first:
        d2.append(index - p2_first + self.zero_position)
      if p2_first <= index and index <= p2_last:
        d2.append(self.zero_position)
      if index > p2_last:
        d2.append(index - p2_last + self.zero_position)

    return d1, d2, cleaned_tokenized_example

  def _padding(self, example):
    if self.pad:
      while len(example) < self.zero_position-2:
        example += ['emptypaddingtag']
    example = ['emptypaddingtag'] + example + ['emptypaddingtag']
    return example

  def form_evaluation_files_keys(self):
    i = 0
    keys = ""
    _, test_labels = self.get_test_data()
    for label in test_labels:
      keys += str(i) + "\t" + label + "\n"
      i+=1
    test_keys_file = open("test_keys_file.txt", "w")
    test_keys_file.write(keys)
    test_keys_file.close()
    i = 0
    keys = ""
    _, train_labels = self.get_train_data()
    for label in train_labels:
      keys += str(i) + "\t" + label + "\n"
      i+=1
    train_keys_file = open("train_keys_file.txt", "w")
    train_keys_file.write(keys)
    train_keys_file.close()
