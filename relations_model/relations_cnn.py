from relations_model.input_container import InputContainer

from keras.layers import Embedding, Dense, MaxPooling1D, Input, GlobalMaxPooling1D, Convolution1D, Cropping1D, Convolution2D, MaxPooling2D, Lambda, Dropout
from keras.layers import concatenate, add, multiply
from keras.regularizers import l2
from keras.models import Model, Sequential
from keras.optimizers import SGD, TFOptimizer
from keras.callbacks import TensorBoard
from keras.initializers import glorot_uniform, uniform
from keras import backend as K
from keras.models import model_from_json

import colorama
from colorama import Fore
from tqdm import *
import numpy as np
import math
import datetime
import json
import operator
import sys
from sklearn.metrics import f1_score

class RelationsCnn:
  def __init__(self, batch_size=1, with_other_class=True):
    self.batch_size = batch_size
    self.with_other_class = with_other_class
    np.random.seed(0)

  def prepare_input(self, emb_name, emb_length, load_full_vocab, dataset_name, zero_position, pad):
    # zero_position is added to all distances, so will use zero_position and to both sides of it
    # 0 row with zeros for padded length
    self.distances_vocab_length = zero_position*2 + 1
    self.input_container = InputContainer()
    self.input_container.load(emb_name=emb_name, emb_length=emb_length, load_full_vocab=load_full_vocab, dataset_name=dataset_name, zero_position=zero_position, pad=pad)

  def load_input(self, input_container, zero_position):
    self.input_container = input_container
    self.distances_vocab_length = zero_position*2 + 1

  def _custom_log_loss(self, y_true, y_pred):
    gamma = 2.0
    m_pos = 2.5
    m_neg = 0.5

    error = 0.0
    with K.tf.name_scope('custom_log_loss'):
      for i in range(self.batch_size):
        # top two values in our sequence
        values, _ = K.tf.nn.top_k(y_pred[i], 2)
        # get true label index
        y_max = K.argmax(y_true[i])
        # predicted score for the true label
        correct_score = K.tf.gather(y_pred[i], y_max)
        # wrong score - maximal after predicted
        wrong_score = K.tf.cond(K.equal(correct_score, values[0]), lambda: values[1], lambda: values[0])
        # trying to avoid overflow - can replace ln(1+e^x) with x, when it is overflowing
        correct_move = gamma*(m_pos - correct_score)
        correct_part = K.tf.cond(K.tf.is_inf(K.exp(correct_move)), lambda: correct_move, lambda: K.log(1 + K.exp(correct_move)))
        wrong_move = gamma*(m_neg + wrong_score)
        wrong_part = K.tf.cond(K.tf.is_inf(K.exp(wrong_move)), lambda: wrong_move, lambda: K.log(1 + K.exp(wrong_move)))
        # if the training example is of Other class, we do not want to train network, only move away the wrong answer
        if not self.with_other_class:
          error = error + K.tf.cond(K.equal(y_max, self.input_container.other_class_index), lambda: wrong_part, lambda: correct_part + wrong_part)
        else:
          error = error + correct_part + wrong_part
      mean_error = error/self.batch_size
    return mean_error

  def init_model(self, char_based, num_layers_hw=2, chars_emb_dim=15, f_sizes=[2,3,4,5], f_filters=[100,150,200,200], distances_emb_length=70, train_words_emb=False, train_distance_emb=True, filters_num=500, dilated_filters_num=500, window_size=3, regulize_rate=0.001):
    st_init = glorot_uniform(seed=0)
    with K.tf.name_scope('main_model'):
      if char_based:
        input1 = Input(shape=(None,self.input_container.word_maxlen))
        char_emb = Embedding(self.input_container.vocab_length, chars_emb_dim, embeddings_initializer=uniform(seed=0), trainable=True)(input1)
        pool_layers = []
        for f_size, f_filter in zip(f_sizes, f_filters):
           conv = Convolution2D(f_filter, f_size, activation='tanh', padding='same', kernel_initializer=st_init)(char_emb)
           pooled = MaxPooling2D((1,self.input_container.word_maxlen))(conv)
           pool_layers.append(Dropout(0.5, seed=0)(pooled))
        char_based_word_emb = concatenate(pool_layers)
        hw_input = Lambda(lambda x: K.squeeze(x, 2))(char_based_word_emb)
      
        print(hw_input)
        for idx in range(num_layers_hw):
          g = Dense(sum(f_filters), activation='relu', kernel_initializer=st_init)(hw_input)
          t = Dense(sum(f_filters), activation='sigmoid', kernel_initializer=st_init)(hw_input)
          t = Lambda(lambda x: x - 2)(t)
          t1 = Lambda(lambda x: 1. - t)(t)
          output = add([multiply([t, g]), multiply([t1, hw_input])])
          hw_input = output
        print(hw_input)
        word_emb = hw_input
      else:
        input1 = Input(shape=(None,))
        word_emb = Embedding(self.input_container.vocab_length, self.input_container.emb_length, weights=[self.input_container.embedding_matrix], trainable=train_words_emb)(input1)

      d_embedding_matrix = np.vstack((np.zeros(distances_emb_length), np.random.rand(self.distances_vocab_length - 1, distances_emb_length)))
      input2 = Input(shape=(None,))
      d1_emb = Embedding(self.distances_vocab_length, distances_emb_length, weights=[d_embedding_matrix], trainable=train_distance_emb)(input2)
      input3 = Input(shape=(None,))
      d2_emb = Embedding(self.distances_vocab_length, distances_emb_length, weights=[d_embedding_matrix], trainable=train_distance_emb)(input3)

      # makes shape (sequnce_length, 1, emb_length + 2*dist_emb_length), i.e. tensor with full embedding on each word
      merged = concatenate([word_emb,d1_emb, d2_emb])
      conv = Convolution1D(filters_num, window_size, activation='tanh', padding='valid', strides=1, kernel_regularizer=l2(regulize_rate), bias_regularizer=l2(regulize_rate), kernel_initializer=st_init)(merged)
      #dilated_conv = Convolution1D(dilated_filters_num, window_size, activation='tanh', padding='causal', strides=1, kernel_regularizer=l2(regulize_rate), bias_regularizer=l2(regulize_rate), dilation_rate=2)(merged)
      #cropped = Cropping1D(cropping=(1,1))(dilated_conv)
      #merged_conv = concatenate([conv, cropped])
      pooling = GlobalMaxPooling1D()(conv) #merged_conv)
      print(pooling)
      # initialization as specified in the paper
      r = math.sqrt(6.0/(self.input_container.n_classes + filters_num + dilated_filters_num))
      # after training weights[0].T will contain embedding of relation in every row, length of each embedding is filters_num
      weights = [np.random.uniform(-r,r,(filters_num + dilated_filters_num, self.input_container.n_classes))]
      if not self.with_other_class:
        weights[0].T[self.input_container.other_class_index] = 0
      scores = Dense(self.input_container.n_classes, weights=weights, use_bias=False, kernel_regularizer=l2(regulize_rate))(pooling)
      print(scores)
      self.model = Model(inputs=[input1,input2,input3], outputs=scores)
      self.model.summary()

    # models for getting representative trigrams
    self.conv_model = Model(inputs=[input1,input2,input3], outputs=conv)
    self.repr_model = Model(inputs=[input1,input2,input3], outputs=pooling)

  def load_model(self, model_file, weights_file):
    saved_model = open(model_file, 'r')
    self.model = model_from_json(json.loads(saved_model.read()))
    self.model.load_weights(weights_file)
    self.model.summary()

  def mil_train(self, epochs_num, start_epoch=0, verbose=True, check_train_accuracy=False):
    assert not self.model is None, "Model was not initialized! Load or initialize model"
    assert not self.input_container.bags is None, "Bags for MIL are not initialized! Form them"

    losses = []
    av_losses = []
    train_accuracies = []
    accuracies = []
    init_learning_rate = 0.025
    startTime = datetime.datetime.now()
    experiment_timestamp = startTime.strftime("%Y%m%d%H%M")
    for ep in range(start_epoch, start_epoch + epochs_num):
      epoch_losses = []
      # sgd = SGD(lr=init_learning_rate/(ep+1), momentum=0.0, decay=0.0, nesterov=False)
      sgd = TFOptimizer(K.tf.train.GradientDescentOptimizer(init_learning_rate/(ep+1)))
      self.model.compile(sgd, self._custom_log_loss)
      # shuffling
      indices = np.arange(len(self.input_container.bags))
      np.random.shuffle(indices)
      shuffled_bags = np.asarray(self.input_container.bags)[indices]

      i = 0
      for i in tqdm(range(shuffled_bags.shape[0])):
        maximal_score = -sys.maxint - 1
        maximal_index = 0
        for index in shuffled_bags[i]:
          seq = self.input_container.sequences[index]
          l = len(seq)
          prediction = self.model.predict([np.asarray(seq).reshape((1,l)), np.asarray(self.input_container.d1s[index]).reshape((1,l)), np.asarray(self.input_container.d2s[index]).reshape((1,l))]).reshape(self.input_container.n_classes)
          prediction = self.preprocess_prediction(prediction)
          if prediction.max() > maximal_score:
            maximal_score = prediction.max()
            maximal_index = index

        seq = self.input_container.sequences[maximal_index]
        l = len(seq)
        if verbose:
          print("\tExample" + Fore.BLUE + str(maximal_index) + Fore.WHITE + "length" + Fore.BLUE + str(l) + Fore.WHITE + ", dist_emb1 length:" + Fore.BLUE + str(len(self.input_container.d1s[maximal_index])) + Fore.WHITE + ", dist_emb2 length:" + Fore.BLUE + str(len(self.input_container.d2s[maximal_index])))

        h = self.model.fit([np.asarray(seq).reshape((1,l)), np.asarray(self.input_container.d1s[maximal_index]).reshape((1,l)), np.asarray(self.input_container.d2s[maximal_index]).reshape((1,l))], self.input_container.train_labels[maximal_index].reshape((1,self.input_container.n_classes)), nb_epoch=1, batch_size=self.batch_size, verbose=0)

        if verbose:
          print("\tEpoch" + Fore.BLUE + str(ep+1) + Fore.WHITE + ", Example" + Fore.BLUE + str(maximal_index) + Fore.WHITE + ", Loss:" + Fore.BLUE + str(h.history['loss'][0]))
        epoch_losses.append(h.history['loss'][0])
        i += 1

      if check_train_accuracy:
        i = 0
        correct_number = 0
        for seq in self.input_container.sequences:
          l = len(seq)
          prediction = self.model.predict([np.asarray(seq).reshape((1,l)), np.asarray(self.input_container.d1s[i]).reshape((1,l)), np.asarray(self.input_container.d2s[i]).reshape((1,l))]).reshape(self.input_container.n_classes)
          prediction = self.preprocess_prediction(prediction)
          if prediction.argmax() == self.input_container.train_labels[i].argmax():
            correct_number += 1
          i += 1
        train_accuracies.append((correct_number*100.0)/len(self.input_container.sequences))
      losses.append(epoch_losses)
      av_loss = np.asarray(epoch_losses).mean()
      av_losses.append(av_loss)
      j = 0
      correct_number = 0
      for seq in self.input_container.test_sequences:
        l = len(seq)
        prediction = self.model.predict([np.asarray(seq).reshape((1,l)), np.asarray(self.input_container.test_d1s[j]).reshape((1,l)), np.asarray(self.input_container.test_d2s[j]).reshape((1,l))]).reshape(self.input_container.n_classes)
        prediction = self.preprocess_prediction(prediction)
        if prediction.argmax() == self.input_container.test_labels[j].argmax():
          correct_number += 1
        j += 1
      accuracy = (correct_number*100.0)/len(self.input_container.test_sequences)
      if check_train_accuracy:
        print(Fore.GREEN + "Train accuracy is: " + str(train_accuracies[-1]))
      print(Fore.BLUE + "Epoch" + str(ep+1) + Fore.GREEN + "Accuracy is:" + str(accuracy) + Fore.RED + "Average loss is:" + str(av_loss))
      accuracies.append(accuracy)
      ep += 1

    print(Fore.YELLOW + "Training time: " + str(datetime.datetime.now() - startTime))

    setup = "_epochs" + str(start_epoch) + "-" + str(start_epoch + epochs_num) + "_" + experiment_timestamp
    losses_file = open("losses" + setup + ".txt", "w")
    losses_file.write("\n".join(np.asarray(losses).flatten().astype('str')))
    losses_file.close()
    av_losses_file = open("avg_losses" + setup + ".txt", "w")
    av_losses_file.write("\n".join(np.asarray(av_losses).astype('str')))
    av_losses_file.close()
    accuracies_file = open("accuracies" + setup + ".txt", "w")
    accuracies_file.write("\n".join(np.asarray(accuracies).astype('str')))
    accuracies_file.close()
    train_accuracies_file = open("train_accuracies" + setup + ".txt", "w")
    train_accuracies_file.write("\n".join(np.asarray(train_accuracies).astype('str')))
    train_accuracies_file.close()
    self.model.save_weights("weights" + setup + ".h5")
    print(Fore.YELLOW + "Training data saved")

  def write_log(self, callback, names, logs, batch_no):
    for name, value in zip(names, logs):
      summary = tf.Summary()
      summary_value = summary.value.add()
      summary_value.simple_value = value
      summary_value.tag = name
      callback.writer.add_summary(summary, batch_no)
      callback.writer.flush()

  def correctness(self, y_true, y_pred):
    y_pred = self.preprocess_prediction(y_pred)
    return K.cast(K.equal(K.argmax(y_true), K.argmax(y_pred)), 'float32')

  def answer(self, y_true, y_pred):
    y_pred = self.preprocess_prediction(y_pred)
    return K.argmax(y_pred)

  def preprocess_prediction(self, prediction):
    if self.with_other_class:
      return prediction
    else:
      ref = K.variable(value=np.zeros((self.input_container.n_classes, self.batch_size)))
      ref = K.tf.assign(ref, K.transpose(prediction))
      indices = K.constant(value=[self.input_container.other_class_index], dtype='int32')
      updates = K.zeros((1, self.batch_size))
      return K.transpose(K.tf.scatter_update(ref, indices, updates))

  def train(self, epochs_num, init_lr=0.025, start_epoch=0, verbose=True):
    assert not self.model is None, "Model was not initialized! Load or initialize model"

    losses = []
    av_losses = []
    train_accuracies = []
    accuracies = []
    test_errors = []
    av_test_errors = []

    startTime = datetime.datetime.now()
    experiment_timestamp = startTime.strftime("%Y%m%d%H%M")

    callback = TensorBoard('./logs/' + experiment_timestamp)
    callback.set_model(self.model)
    train_names = ['train_loss']
    test_names = ['test_loss']

    test_int_labels = np.argmax(self.input_container.test_labels,axis=1)
    test_score_indexes = np.argwhere(test_int_labels!=self.input_container.other_class_index).flatten()

    for ep in range(start_epoch, start_epoch + epochs_num):
      print(Fore.BLUE + "Epoch " + str(ep+1))
      # slower Keras optimizer implementation
      # sgd = SGD(lr=init_lr/(ep+1), momentum=0.0, decay=0.0, nesterov=False)
      sgd = TFOptimizer(K.tf.train.GradientDescentOptimizer(init_lr/(ep+1)))
      self.model.compile(sgd, self._custom_log_loss, metrics=[self.correctness, self.answer])
      # shuffling
      indices = np.arange(len(self.input_container.sequences))
      np.random.shuffle(indices)
      shuffled_sequences = np.asarray(self.input_container.sequences)[indices]
      shuffled_train_labels = self.input_container.train_labels[indices]
      shuffled_d1s = np.asarray(self.input_container.d1s)[indices]
      shuffled_d2s = np.asarray(self.input_container.d2s)[indices]

      if self.batch_size > 1:
        border = (shuffled_sequences.shape[0]//self.batch_size) * self.batch_size
        h = self.model.fit([shuffled_sequences[:border], shuffled_d1s[:border], shuffled_d2s[:border]], shuffled_train_labels[:border], batch_size=self.batch_size, epochs=1, shuffle=False)

        av_losses.append(h.history['loss'][0])
        train_accuracies.append(h.history['correctness'][0]*100.0)
        print(Fore.GREEN + "Train accuracy (micro F-score with Other):" + str(train_accuracies[-1]) + Fore.RED + "Average train loss:" + str(av_losses[-1]))
      else:
        i = 0
        epoch_losses = []
        correct_predictions = 0
        answers = []
        for i in tqdm(range(shuffled_sequences.shape[0])):
          seq = shuffled_sequences[i]
          l = len(seq)
          seq = np.asarray(seq).reshape((1,) + np.asarray(seq).shape)
          d1 = np.asarray(shuffled_d1s[i]).reshape((1,-1))
          d2 = np.asarray(shuffled_d2s[i]).reshape((1,-1))
          label = shuffled_train_labels[i].reshape((1,self.input_container.n_classes))

          if verbose:
            print("\tExample" + Fore.BLUE + str(indices[i]) + Fore.WHITE + ", length" + Fore.BLUE + str(l) + Fore.WHITE + ", dist_emb1 length:" + Fore.BLUE + str(len(shuffled_d1s[i])) + Fore.WHITE + ", dist_emb2 length:" + Fore.BLUE + str(len(shuffled_d2s[i])))

          # write_log(callback, train_names, logs, i)
          h = self.model.train_on_batch([seq, d1, d2], label)

          if verbose:
            print("Loss:" + Fore.BLUE + str(h[0]))

          epoch_losses.append(h[0])
          correct_predictions += h[1]
          answers.append(h[2])
          i += 1

        int_labels = np.argmax(shuffled_train_labels,axis=1)
        score_indexes = np.argwhere(int_labels!=self.input_container.other_class_index).flatten()
        macro_fscore = f1_score(int_labels[score_indexes], np.asarray(answers)[score_indexes], average='macro')
        losses.append(epoch_losses)
        av_loss = np.asarray(epoch_losses).mean()
        av_losses.append(av_loss)
        train_accuracies.append((correct_predictions*100.0)/len(self.input_container.sequences))
        print(Fore.GREEN + "Train accuracy (micro F-score with Other):" + str(train_accuracies[-1]) + "Train macro F-score without Other:" + str(macro_fscore*100) + Fore.RED + "Average train loss:" + str(av_loss))

      if self.batch_size > 1:
        border = (self.input_container.test_sequences.shape[0]//self.batch_size) * self.batch_size
        h = self.model.evaluate([self.input_container.test_sequences[:border], self.input_container.test_d1s[:border], self.input_container.test_d2s[:border]], self.input_container.test_labels[:border], batch_size=self.batch_size)

        av_test_errors.append(h[0])
        accuracies.append(h[1]*100.0)

        print(Fore.GREEN + "Test accuracy (micro F-score with Other):" + str(accuracies[-1]) + Fore.RED + "Average test loss:" + str(av_test_errors[-1]))
      else:
        j = 0
        correct_number = 0
        epoch_errors = []
        answers = []
        for seq in self.input_container.test_sequences:
          seq = np.asarray(seq).reshape((1,) + np.asarray(seq).shape)
          d1 = np.asarray(self.input_container.test_d1s[j]).reshape((1,-1))
          d2 = np.asarray(self.input_container.test_d2s[j]).reshape((1,-1))
          label = self.input_container.test_labels[j].reshape((1,self.input_container.n_classes))
          h = self.model.test_on_batch([seq, d1, d2], label)
          # write_log(callback, test_names, logs, j)
          correct_number += h[1]
          epoch_errors.append(h[0])
          answers.append(h[2])
          j += 1

        macro_fscore = f1_score(test_int_labels[test_score_indexes], np.asarray(answers)[test_score_indexes], average='macro')
        test_errors.append(epoch_errors)
        av_test_error = np.asarray(epoch_errors).mean()
        av_test_errors.append(av_test_error)
        accuracy = (correct_number*100.0)/len(self.input_container.test_sequences)
        accuracies.append(accuracy)

        print(Fore.GREEN + "Test accuracy (micro F-score with Other):" + str(accuracy) + "Test macro F-score without Other:" + str(macro_fscore*100) + Fore.RED + "Average test loss:" + str(av_test_error))
      ep += 1

    print(Fore.YELLOW + "Training time: " + str(datetime.datetime.now() - startTime))

    setup = "_epochs" + str(start_epoch) + "-" + str(start_epoch + epochs_num) + "_" + experiment_timestamp
    losses_file = open("losses" + setup + ".txt", "w")
    losses_file.write("\n".join(np.asarray(losses).flatten().astype('str')))
    losses_file.close()
    av_losses_file = open("avg_losses" + setup + ".txt", "w")
    av_losses_file.write("\n".join(np.asarray(av_losses).astype('str')))
    av_losses_file.close()
    accuracies_file = open("accuracies" + setup + ".txt", "w")
    accuracies_file.write("\n".join(np.asarray(accuracies).astype('str')))
    accuracies_file.close()
    train_accuracies_file = open("train_accuracies" + setup + ".txt", "w")
    train_accuracies_file.write("\n".join(np.asarray(train_accuracies).astype('str')))
    train_accuracies_file.close()
    test_errors_file = open("test_errors" + setup + ".txt", "w")
    test_errors_file.write("\n".join(np.asarray(test_errors).flatten().astype('str')))
    test_errors_file.close()
    av_test_errors_file = open("avg_test_errors" + setup + ".txt", "w")
    av_test_errors_file.write("\n".join(np.asarray(av_test_errors).astype('str')))
    av_test_errors_file.close()
    self.model.save_weights("weights" + setup + ".h5")
    print(Fore.YELLOW + "Training data saved")

  def save_model(self):
    cur_time = datetime.datetime.now()
    save_timestamp = cur_time.strftime("%Y%m%d%H%M")
    with open('model' + save_timestamp + '.txt', 'w') as outfile:
      json.dump(self.model.to_json(), outfile)

  def form_evaluation_files(self):
    cur_time = datetime.datetime.now()
    evaluation_timestamp = cur_time.strftime("%Y%m%d%H%M")
    i = 0
    preds = ""
    for seq in self.input_container.test_sequences:
      l = len(seq)
      seq = np.asarray(seq).reshape((1,) + np.asarray(seq).shape)
      prediction = self.model.predict([seq, np.asarray(self.input_container.test_d1s[i]).reshape((1,l)), np.asarray(self.input_container.test_d2s[i]).reshape((1,l))]).reshape(self.input_container.n_classes)
      if not self.with_other_class:
        prediction[self.input_container.other_class_index] = 0
      preds += str(i) + "\t" + self.input_container.labels_list[prediction.argmax()] + "\n"
      i+=1
    test_preds_file = open("test_preds_file_" + evaluation_timestamp + ".txt", "w")
    test_preds_file.write(preds)
    test_preds_file.close()
    i = 0
    preds = ""
    for seq in self.input_container.sequences:
      l = len(seq)
      seq = np.asarray(seq).reshape((1,) + np.asarray(seq).shape)
      prediction = self.model.predict([seq, np.asarray(self.input_container.d1s[i]).reshape((1,l)), np.asarray(self.input_container.d2s[i]).reshape((1,l))]).reshape(self.input_container.n_classes)
      if not self.with_other_class:
        prediction[self.input_container.other_class_index] = 0
      preds += str(i) + "\t" + self.input_container.labels_list[prediction.argmax()] + "\n"
      i+=1
    train_preds_file = open("train_preds_file" + evaluation_timestamp + ".txt", "w")
    train_preds_file.write(preds)
    train_preds_file.close()

  def get_representative_trigrams(self, trigrams_to_save=5):
    cur_time = datetime.datetime.now()
    evaluation_timestamp = cur_time.strftime("%Y%m%d%H%M")

    representative_trigrams = {}
    for i in tqdm(range(self.input_container.sequences.shape[0])):
      seq = self.input_container.sequences[i]
      # trigrams are in the sentence without padding tags on the borders
      l = len(seq) - 2
      seq = np.asarray(seq).reshape((1,) + np.asarray(seq).shape)
      d1 = np.asarray(self.input_container.d1s[i]).reshape((1,-1))
      d2 = np.asarray(self.input_container.d2s[i]).reshape((1,-1))
      convolution_output = np.squeeze(self.conv_model.predict([seq, d1, d2]))
      sentence_repr = np.squeeze(self.repr_model.predict([seq, d1, d2]))
      prediction = np.squeeze(self.model.predict([seq, d1, d2]))
      predicted_class = prediction.argmax()

      # do not need trigrams for Other class
      if predicted_class == self.input_container.other_class_index:
        continue
      # TODO think of what can be done in this case to save this information
      # do not want to call representative trigrams that were a sign of wrong answer
      if predicted_class != self.input_container.train_labels[i].argmax():
        continue

      trigram_filters = {}
      filter_index = 0
      # iterate through all filter values obtained from the sentence
      # collect for every trigram filters that got maximal value in it
      for filter_row in convolution_output.T:
        trigram_index = filter_row.argmax()
        if trigram_index in trigram_filters:
          trigram_filters[trigram_index].append(filter_index)
        else:
          trigram_filters[trigram_index]= [filter_index]
        filter_index = filter_index + 1

      # vector of the length filters_num
      # vector corresponding to the predicted class in the Dense layer weights matrix
      predicted_class_emb = self.model.get_weights()[-1].T[predicted_class]
      trigram_scores = np.zeros(l)
      for trigram_index in trigram_filters:
        # for every trigram sum up the values that would be produced from the filters reacted on it
        for filter_index in trigram_filters[trigram_index]:
          trigram_scores[trigram_index] += sentence_repr[filter_index]*predicted_class_emb[filter_index]

      text = self.input_container.train_data_texts[i][1:-1]
      # select the trigram with maximal input to the final score
      representative_trigram = trigram_scores.argmax()
      if representative_trigram == 0:
        trigram_start = 0
      else:
        trigram_start = representative_trigram - 1
      trigram = " ".join(text[trigram_start:representative_trigram+2])
      class_name = self.input_container.labels_list[predicted_class]
      # collect trigrams for classes
      # can be two approaches - either to concentrate on the frequency of trigram, or on the value it gives
      if class_name in representative_trigrams:
        if trigram in representative_trigrams[class_name]:
          representative_trigrams[class_name][trigram] += trigram_scores.max()
        else:
          representative_trigrams[class_name][trigram] = trigram_scores.max()
      else:
        representative_trigrams[class_name] = { trigram: trigram_scores.max() }

    outfile = open("representative_trigrams" + evaluation_timestamp + ".txt", "w")
    for class_name in [pair[0] for pair in sorted(representative_trigrams.items(), key=operator.itemgetter(0))]:
      sorted_trigrams = sorted(representative_trigrams[class_name].items(), key=operator.itemgetter(1), reverse=True)
      most = [t[0] for t in sorted_trigrams][0:trigrams_to_save]
      outfile.write(class_name + ': ' + ';'.join(most) + '\n')
    outfile.close()
