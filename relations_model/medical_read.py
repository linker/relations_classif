import pandas as pd
from nltk import word_tokenize
import numpy as np
import string
import re
import spacy
import unicodedata
import codecs
from gensim.models.phrases import Phraser
nlp = spacy.load("en",entity=False)

# reader expects file in the following format:
# line with a sentence with entities marked <e1></e1> and <e2></e2> in double quotes
# line with class name
# empty line
class MedicalRead:
  def __init__(self, dataset_dir, zero_position=100, pad=False):
    self.dataset = dataset_dir
    self.longest_sentence = 0
    self.zero_position = zero_position
    self.pad = pad
    gfile = codecs.open("medical_preproc/gene_names.txt",'rb',encoding="utf8")
    self.genes = set(gfile.read().split("\n"))
    self.bigram_phraser = Phraser.load("medical_preproc/bigram_phraser_pubmed")
    self.trigram_phraser = Phraser.load("medical_preproc/trigram_phraser_pubmed")

  def smart_lowercase(self,token):
    # If token is a gene return original token text, else lowercase
    if token in self.genes:
        return token
    else:
        return token.lower()

  # returns two arrays
  # first is of triples - sentence (tokenized and downcased) and two of distances - to e1 and to e2
  # second just of labels
  def get_train_data(self):
    all_train_data = pd.read_csv(self.dataset + '/train.txt', sep="\n", header = None, quoting = 3)
    return self._get_formatted_examples(all_train_data)

  def get_test_data(self):
    all_test_data = pd.read_csv(self.dataset + '/test.txt', sep="\n", header = None, quoting = 3)
    return self._get_formatted_examples(all_test_data)

  def get_any_data(self, data_tagged):
    data = []
    for example in data_tagged:
      #TODO are there any problems with non-ascii?
      #example = self._remove_non_ascii(example)
      d1, d2, tokenized_example = self._get_distance_arrays(example)
      data.append([tokenized_example, d1, d2])
      if len(tokenized_example) > self.longest_sentence:
        self.longest_sentence = len(tokenized_example)

    return data

  def _get_formatted_examples(self, read_csv):
    # get sentences (every 2nd is a sentence), transform to numpy array
    data_tagged = read_csv[0][::2].as_matrix()
    data = []
    for example in data_tagged:
      #TODO are there any problems with non-ascii?
      #example = self._remove_non_ascii(example)
      #example = example[1:-1]
      d1, d2, tokenized_example = self._get_distance_arrays(example)
      data.append([tokenized_example, d1, d2])
      if len(tokenized_example) > self.longest_sentence:
        self.longest_sentence = len(tokenized_example)

    # labels are every 3d starting from second line
    labels = read_csv[0][1::2].as_matrix()
    return data, labels

  def _remove_non_ascii(self, s):
    return "".join(i for i in s if ord(i)<128)

  def _get_distance_arrays(self, example):
    # replace < and > in entities tags with @@@, so word_tokenizer will not place them to different tokens
    example = unicode(re.sub(r'<(/?e[12])>', r' @@@\1@@@ ', example))
    replacers = ['@@@e1@@@', '@@@e2@@@', '@@@/e1@@@', '@@@/e2@@@']
    # find positions of the entitites and indexes array for the tokenized example
    i = 0
    indexes = []
    cleaned_tokenized_example = []
    example = " ".join([token.text for token in nlp(example)])

    ntext = re.sub(pattern=r"(\s)[0-9]+\.?[0-9]*\s?\%(\s)",repl="r\1@PERCENT@\2",string=example)
    ntext = re.sub(pattern=r"(\s)[0-9]+\.[0-9]+(\s)",repl=r"\1@DECIMAL@\2",string=ntext)
    ntext = re.sub(pattern=r"(\s)[0-9]+(\s)",repl=r"\1@NUM@\2",string=ntext)

    tokenized_example = [self.smart_lowercase(t) for t in ntext.split(' ')]
    tokenized_example = self.bigram_phraser[tokenized_example]
    tokenized_example = self.trigram_phraser[tokenized_example]
    tokenized_example = [unicodedata.normalize('NFKD', t).encode('ascii','ignore') for t in tokenized_example]
    tokenized_example = self._padding(tokenized_example)
    for word in tokenized_example:
      if word == replacers[0]:
        p1_first = i
      if word == replacers[1]:
        p2_first = i
      if word == replacers[2]:
        p1_last = i-1
      if word == replacers[3]:
        p2_last = i-1
      if word not in replacers:
        cleaned_tokenized_example.append(word)
        indexes.append(i)
        i = i+1
    # form distances arrays
    d1 = []
    d2 = []
    for index in indexes:
      if index < p1_first:
        d1.append(index - p1_first + self.zero_position)
      if p1_first <= index and index <= p1_last:
        d1.append(self.zero_position)
      if index > p1_last:
        d1.append(index - p1_last + self.zero_position)

      if index < p2_first:
        d2.append(index - p2_first + self.zero_position)
      if p2_first <= index and index <= p2_last:
        d2.append(self.zero_position)
      if index > p2_last:
        d2.append(index - p2_last + self.zero_position)

    return d1, d2, cleaned_tokenized_example

  def _padding(self, example):
    if self.pad:
      while len(example) < self.zero_position-2:
        example += ['emptypaddingtag']
    example = ['emptypaddingtag'] + example + ['emptypaddingtag']
    return example

  def form_evaluation_files_keys(self):
    i = 0
    keys = ""
    _, test_labels = self.get_test_data()
    for label in test_labels:
      keys += str(i) + "\t" + label + "\n"
      i+=1
    test_keys_file = open("test_keys_file.txt", "w")
    test_keys_file.write(keys)
    test_keys_file.close()
    i = 0
    keys = ""
    _, train_labels = self.get_train_data()
    for label in train_labels:
      keys += str(i) + "\t" + label + "\n"
      i+=1
    train_keys_file = open("train_keys_file.txt", "w")
    train_keys_file.write(keys)
    train_keys_file.close()
