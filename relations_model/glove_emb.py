import numpy as np

class GloveEmb:
  def __init__(self, emb_length=300):
    #self.emb_path = "glove.6B/glove.6B." + str(emb_length) + "d.txt"
    self.emb_path = "glove_pubmed/glove_pubmed." + str(emb_length) + ".txt"

  def get_emb_index(self):
    embeddings_index = {}
    f = open(self.emb_path)
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    f.close()

    return embeddings_index
