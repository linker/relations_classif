from relations_model.embeddings import Embeddings
from relations_model.dataset import Dataset

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical

import colorama
from colorama import Fore
import numpy as np

class InputContainer:
  def load(self, emb_name, emb_length, dataset_name, zero_position, load_full_vocab, pad):
    self.emb_name = emb_name
    self.emb_length = emb_length
    self.dataset_name = dataset_name
    self.pad = pad

    self.load_embeddings(load_full_vocab)
    train_data, train_labels, test_data, test_labels = self.load_dataset(zero_position, pad)
    self.prepare_data(train_data, train_labels, test_data, test_labels)

  def load_embeddings(self, load_full_vocab):
    self.embeddings = Embeddings(self.emb_name, self.emb_length, load_full_vocab)
    self.embeddings.load()

  def load_dataset(self, zero_position, pad):
    self.dataset = Dataset(self.dataset_name, zero_position, pad)
    return self.dataset.load()

  def prepare_eval_data(self, data):
    print(Fore.YELLOW + "Start transforming data for evaluation...")

    self.eval_data_texts = [t[0] for t in data]

    if not self.embeddings.load_full_vocab:
        self.eval_sequences = []
        for text in self.eval_data_texts:
          cur_seq = []
          for word in text:
             cur_seq.append(self.tokenizer.word_index.get(word, 0))
          self.eval_sequences.append(cur_seq)
        self.eval_sequences = np.asarray(self.eval_sequences)

    self.eval_d1s = np.asarray([t[1] for t in data])
    self.eval_d2s = np.asarray([t[2] for t in data])

    print(Fore.GREEN + "Eval data prepared")

  def prepare_char_data(self, train_data, train_labels, test_data, test_labels, additional_texts=[]):
    print(Fore.YELLOW + "Start transforming train and test data for experiments...")

    self.train_data_texts = [t[0] for t in train_data]
    self.test_data_texts = [t[0] for t in test_data]

    longest_train_word = len(max(np.asarray(self.train_data_texts).flatten(), key=len))
    longest_test_word = len(max(np.asarray(self.test_data_texts).flatten(), key=len))
    if len(additional_texts) > 0:
      longest_additional_word = len(max(np.asarray(additional_texts).flatten(), key=len))
    else:
      longest_additional_word = 0
    self.word_maxlen = max(longest_train_word, longest_test_word, longest_additional_word)

    tokenizer = Tokenizer(filters=self._empty_filter(), char_level=True)
    # create indexes
    texts = self.train_data_texts + self.test_data_texts + additional_texts
    _ = [tokenizer.fit_on_texts(t) for t in texts]
    # convert to sequences of indexes of chars
    self.sequences = np.asarray([pad_sequences(tokenizer.texts_to_sequences(t), maxlen=self.word_maxlen, padding='post') for t in self.train_data_texts])
    self.test_sequences = np.asarray([pad_sequences(tokenizer.texts_to_sequences(t), maxlen=self.word_maxlen, padding='post') for t in self.test_data_texts])
    print(Fore.BLUE + "\tTrain sequences shape" + str(self.sequences.shape))
    print(Fore.BLUE + "\tTest sequences shape" + str(self.test_sequences.shape))

    word_index = tokenizer.word_index
    # adding one for not found embedding
    self.vocab_length = len(word_index) + 1
    print(Fore.BLUE + "\tCharacters in vocabulary" + str(self.vocab_length))

    self.d1s = np.asarray([t[1] for t in train_data])
    self.d2s = np.asarray([t[2] for t in train_data])

    self.test_d1s = np.asarray([t[1] for t in test_data])
    self.test_d2s = np.asarray([t[2] for t in test_data])

    # contains correspondence of labels names to labels indexes
    self.labels_list = list(set(np.concatenate((train_labels,test_labels))))
    self.other_class_index = self.labels_list.index('Other')
    self.n_classes = len(self.labels_list)
    # transform to integers
    train_labels = [self.labels_list.index(label) for label in train_labels]
    # trainsform to one-hot-encoded
    self.train_labels = to_categorical(np.asarray(train_labels))
    print(Fore.BLUE + "\tTrain labels shape" + str(self.train_labels.shape))

    test_labels = [self.labels_list.index(label) for label in test_labels]
    self.test_labels = to_categorical(np.asarray(test_labels))
    print(Fore.BLUE + "\tTest labels shape" + str(self.test_labels.shape))

    print(Fore.GREEN + "Test and train data prepared")

  def prepare_data(self, train_data, train_labels, test_data, test_labels, additional_texts=[]):
    print(Fore.YELLOW + "Start transforming train and test data for experiments...")

    self.train_data_texts = [t[0] for t in train_data]
    self.test_data_texts = [t[0] for t in test_data]

    if self.embeddings.load_full_vocab:
      self._transform_to_sequences_with_full_vacabulary()
    else:
      self._transform_to_sequences(additional_texts)

    self.embeddings.embeddings_index = None

    self.d1s = np.asarray([t[1] for t in train_data])
    self.d2s = np.asarray([t[2] for t in train_data])

    self.test_d1s = np.asarray([t[1] for t in test_data])
    self.test_d2s = np.asarray([t[2] for t in test_data])

    # contains correspondence of labels names to labels indexes
    self.labels_list = list(set(np.concatenate((train_labels,test_labels))))
    self.other_class_index = self.labels_list.index('Other')
    self.n_classes = len(self.labels_list)
    # transform to integers
    train_labels = [self.labels_list.index(label) for label in train_labels]
    # trainsform to one-hot-encoded
    self.train_labels = to_categorical(np.asarray(train_labels))
    print(Fore.BLUE + "\tTrain labels shape" + str(self.train_labels.shape))

    test_labels = [self.labels_list.index(label) for label in test_labels]
    self.test_labels = to_categorical(np.asarray(test_labels))
    print(Fore.BLUE + "\tTest labels shape" + str(self.test_labels.shape))

    print(Fore.GREEN + "Test and train data prepared")

  # assuming that each pair is only in one class - so we do not need to give labels to bags,
  # it just follows from pair
  def make_mi_bags(self, zero_position=100):
    self.train_labels
    bags_of_ind = {}
    for i in range(len(self.train_data_texts)):
        words = np.array(self.train_data_texts[i])
        e1 = ' '.join(words[np.where(np.array(self.d1s[i]) == zero_position)[0]])
        e2 = ' '.join(words[np.where(np.array(self.d2s[i]) == zero_position)[0]])
        # might be changed in order to make bags of desired size
        if bags_of_ind.get(e1 + '~' + e2) == None:
            if bags_of_ind.get(e2 + '~' + e1) == None:
                bags_of_ind[e1 + '~' + e2] = []
                bags_of_ind[e1 + '~' + e2].append(i)
            else:
                bags_of_ind[e2 + '~' + e1].append(i)
        else:
            bags_of_ind[e1 + '~' + e2].append(i)
    self.bags = []
    for pair in bags_of_ind:
        self.bags.append(bags_of_ind[pair])

  def _transform_to_sequences(self, additional_texts):
    self.tokenizer = Tokenizer(filters=self._empty_filter())
    # create indexes
    texts = self.train_data_texts + self.test_data_texts + additional_texts
    print(texts)
    self.tokenizer.fit_on_texts([' '.join(t) for t in texts])
    # convert to sequences of indexes of the words
    self.sequences = np.asarray(self.tokenizer.texts_to_sequences([' '.join(t) for t in self.train_data_texts]))
    self.test_sequences = []
    for test_text in self.test_data_texts:
      cur_seq = []
      for word in test_text:
         cur_seq.append(self.tokenizer.word_index.get(word, 0))
      self.test_sequences.append(cur_seq)
    self.test_sequences = np.asarray(self.test_sequences)
    #self.test_sequences = np.asarray(self.tokenizer.texts_to_sequences([' '.join(t) for t in self.test_data_texts]))
    print(Fore.BLUE + "\tTrain sequences shape" + str(self.sequences.shape))
    print(Fore.BLUE + "\tTest sequences shape" + str(self.test_sequences.shape))

    word_index = self.tokenizer.word_index
    # adding one for not found embedding
    self.vocab_length = len(word_index) + 1
    print(Fore.BLUE + "\tWords in vocabulary" + str(self.vocab_length))

    print(Fore.YELLOW + "Start forming only this dataset embeddings matrix...")
    self.embedding_matrix = np.random.rand(self.vocab_length, self.emb_length)
    not_found = 0
    for word, i in word_index.items():
      embedding_vector = self.embeddings.embeddings_index.get(word)
      if embedding_vector is not None:
        # words not found in embedding index will be randomly initialized
        self.embedding_matrix[i] = embedding_vector
      else:
        not_found = not_found + 1

    print(Fore.BLUE + "\tOverall unique words count" + Fore.GREEN + str(len(word_index)) + Fore.BLUE + ", not found in vocabulary words count" + Fore.RED + str(not_found))
    print(Fore.GREEN + "Local embeddings matrix shape" + str(len(self.embedding_matrix)) + " " + str(len(self.embedding_matrix[0])))

  def _empty_filter(self):
    return ''

  def _transform_to_sequences_with_full_vacabulary(self):
    assert not self.embeddings.vocab is None, "Full vocabulary was not initialized! Load embeddings with load_full_vocab option set to True"

    train_not_found, train_overall_count, self.sequences = self._full_vocab_seq_helper(self.train_data_texts)
    test_not_found, test_overall_count, self.test_sequences = self._full_vocab_seq_helper(self.test_data_texts)
    print(Fore.BLUE + "\tTrain sequences shape" + str(self.sequences.shape))
    print(Fore.BLUE + "\tTest sequences shape" + str(self.test_sequences.shape))

    print(Fore.YELLOW + "Start forming full (from word embeddings vocabulary) embeddings matrix...")
    self.embedding_matrix = np.random.rand(self.embeddings.vocab_length, self.emb_length)
    self.vocab_length = self.embeddings.vocab_length
    for word, embedding in self.embeddings.embeddings_index.items():
      self.embedding_matrix[self.embeddings.vocab[word]] = embedding

    print(Fore.GREEN + "Full embeddings matrix shape" + str(len(self.embedding_matrix)) + " " + str(len(self.embedding_matrix[0])))

    print(Fore.BLUE + "\tOverall words count" + Fore.GREEN + str(train_overall_count+test_overall_count) + Fore.BLUE + ", not found in vocabulary words count" + Fore.RED + str(train_not_found+test_not_found))

  def _full_vocab_seq_helper(self, texts):
    sequences = []
    not_found = 0
    overall_count = 0
    for t in texts:
      seq = []
      for w in t:
        if self.embeddings.vocab.get(w) is not None:
          seq.append(self.embeddings.vocab.get(w))
        else:
          seq.append(0)
          not_found = not_found + 1
        overall_count = overall_count + 1
      sequences.append(seq)
    return not_found, overall_count, np.asarray(sequences)
