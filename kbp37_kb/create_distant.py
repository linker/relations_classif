import csv
import codecs
import re
from whoosh.qparser import QueryParser
from whoosh.index import open_dir
import os

per_class_limit = 3000
other_limit = 8000
for rel_csv in os.listdir('clean_united_entity_pairs'):
  dataset = codecs.open('dataset_' + rel_csv + '.txt', 'w', encoding='utf-8')
  pairs_dict = {}
  relation1_counter = 0
  relation2_counter = 0
  sentences = []
  for indexdir in os.listdir('/data/user/ladilova/NYT_corpus/indexes'):
    ix = open_dir(os.path.join('/data/user/ladilova/NYT_corpus/indexes', indexdir))
    parser = QueryParser("content", ix.schema)

    with open(os.path.join('clean_united_entity_pairs', rel_csv), 'rb') as csvf:
      kb_reader = csv.reader(csvf, delimiter='\t')
      for row in kb_reader:
        # print row
        # read entities and relation from KB
        e1 = row[1]
        e2 = row[2]
        relation = row[0]

        if relation1_counter >= per_class_limit and relation2_counter >= per_class_limit and relation!='Other':
          break
        if relation1_counter >= other_limit and relation=='Other':
          break
        # create query for searching entities as phrases in the text
        myquery = parser.parse('"%s" "%s"' %(e1, e2))
        with ix.searcher() as searcher:
          results = searcher.search(myquery, limit=100)
          for result in results:
            # result may contain new lines, when sentences were badly formed
            for sent in result['content'].split('\n'):
              # there are might be bad sentences, containing trash, they are usually very long
              if len(sent) > 2000:
                continue
              # not to fail on encoding, just omit the sentence
              try:
                # try to find and replace one (!) first entity
                p1 = re.compile(r'\b%s\b' % (e1), re.IGNORECASE)
                sent = p1.sub("<e1>%s</e1>" % (e1), sent, count=1)
                e1_ind = sent.find('<e1>')
                # if no first entity, then maybe it was a part of sentence without entities on a new line
                if e1_ind < 0:
                  continue

                # try to find one (!) second entity in the sentence after the first entity
                p2 = re.compile(r'\b%s\b' % (e2), re.IGNORECASE)
                sent = sent[:e1_ind+2*4+len(e1)] + p2.sub("<e2>%s</e2>" % (e2), sent[e1_ind+2*4+len(e1):], count=1)
                e2_ind = sent.find('<e2>')
                # if there is no, try to find before
                if e2_ind < 0:
                  sent = p2.sub(" <e2>%s</e2> " % (e2), sent[:e1_ind], count=1) + sent[e1_ind:]
                  e2_ind = sent.find('<e2>')
                # if still no second entity - there were overlapping or something else went wrong, omit the result
                if e2_ind < 0:
                  continue

                # a lot of sentences are repeating, to avoid it check if we already have the same sentence
                if sent in sentences:
                  break

                quant = pairs_dict.get(e1 + ';' + e2)
                if quant==5:
                  continue
                else:
                  if quant == None:
                    pairs_dict[e1 + ';' + e2] = 1
                  else:
                    pairs_dict[e1 + ';' + e2] += 1

                  # when both entities found and highlighted add the sentence to the base
                  # print sent
                  # for each relation we have two variants (except Other), depending on what entity is the first in the sentence
                  sentences.append(sent)
                  if relation=='Other' and relation1_counter < other_limit:
                    dataset.write('"%s"\n' % (sent))
                    dataset.write('%s\n' % (relation))
                    dataset.write('\n')
                    relation1_counter += 1
                  else:
                    if e1_ind > e2_ind and relation1_counter < per_class_limit:
                      sent = sent.replace('<e1>', '<e3>')
                      sent = sent.replace('</e1>', '</e3>')
                      sent = sent.replace('<e2>', '<e1>')
                      sent = sent.replace('</e2>', '</e1>')
                      sent = sent.replace('<e3>', '<e2>')
                      sent = sent.replace('</e3>', '</e2>')
                      dataset.write('"%s"\n' % (sent))
                      dataset.write('%s(e2,e1)\n' % (relation))
                      dataset.write('\n')
                      relation1_counter += 1
                    else:
                      if relation2_counter < per_class_limit:
                        dataset.write('"%s"\n' % (sent))
                        dataset.write('%s(e1,e2)\n' % (relation))
                        dataset.write('\n')
                        relation2_counter += 1
              except:
                continue
