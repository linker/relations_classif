f1 = open("dwd_entity_pairs/02_per:alternate_names", "r")

dwd_lines = []

for line in f1.readlines():
  name1 = line.split('\t')[1]
  name2 = line.split('\t')[2]
  if len(name1)==1 or len(name2)==1 or len(name1.split(' '))==1:
    continue
  else:
    dwd_lines.append(line)


for line in f1.readlines():
  dwd_lines.append(line)

f2 = open("entity_pairs/per_alternate_names(e1,e2).csv", "r")

wiki_lines = []

for line in f2.readlines():
  name1 = line.split('\t')[1]
  name2 = line.split('\t')[2]
  if name1[0]=='"' and name1[-1]=='"':
    name1 = name1[1:-1]
  if name2[0]=='"' and name2[-2]=='"':
    name2 = name2[1:-2] + '\n'
  wiki_lines.append(line.split('\t')[0] + '\t' + name1 + '\t' + name2)


with open("clean_united_entity_pairs/19_other.txt", "w") as outp:
  for line in wiki_lines:
    outp.write(line)
  for line in dwd_lines:
    outp.write(line)
