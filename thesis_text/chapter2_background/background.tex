\chapter{Background} 
\label{chapter:background}

Nowadays actively used methods for Relation Extraction in biomedical domain are mostly based 
on hand-crafted features, kernels or use multiple Natural Language Processing methods for various parsing and labelling before 
classification itself. The goal is to reduce the need of additional preprocessing to the minimum, thus 
the approaches based on Convolutional Neural Network were preferred. Convolutional Neural Networks are known for their 
capability of finding features and making decisions based on the most important ones. Furthermore, 
as deep learning models normally require a large amount of training data and experts' labor is 
costly, the methods to automatically obtain training data were studied. One of the most popular 
approaches in the field of Relation Extraction is Distant Supervision. And it can be further 
improved in different ways to obtain better results.

\section{Summary of relevant approaches}
As the task of Relation Classification appeared in the Natural Language Processing
field, different kinds of hand-crafted linguistic features together with simple classifiers 
were applied to solving it. According to a recent survey on Relation Extraction
 in the medical domain \cite{Luo05022016}, a general pipeline for extracting and classifying relations 
looks like the one depicted in the Figure \ref{fig:general_pipeline}. The state-of-the-art methods 
are based on structural analysis and classification of the dependency trees obtained 
from the texts by means of Natural Language Processing methods.

	\begin{figure}
		\centering
		\includegraphics[width=\linewidth]{chapter2_background/images/general_pipeline}
		\caption[General pipeline for Relation Extraction]{General pipeline for Relation Extraction and classification in medical domain (from \cite{Luo05022016}).}
		\label{fig:general_pipeline}
	\end{figure}
	
Using hand-crafted domain specific features for classification was also observed in other
fields, such as image classification and speech recognition. 
Nowadays the methods based on deep neural networks are
state-of-the-art in those fields. This fact 
inspired the use of similar methods in the task of Relation Classification. 
Usage of deep learning in the 
context of Natural Language Processing tasks requires representing the text in the format suitable for feeding as an input 
to neural networks. As of late, the most used and powerful representations are words embeddings, e.g. Word2Vec \cite{NIPS2013_5021}, GloVe \cite{pennington2014glove}. 
Another interesting form of embeddings is a recent work by Google named Swivel \cite{DBLP:journals/corr/ShazeerDEW16}. 

One of the papers on the topic of application of Convolutional Neural Network for Relation Classification 
\cite{a27169dffe4642f78115676ce777c52c} suggests using a simple network with 
one convolutional layer. It captures features of different window sizes (2, 3, 4, 5), applies  
global max pooling and passes the results to softmax classification. 
Global max pooling helps to ignore different sizes of input sentences. Better results
were achieved by using a different approach than softmax 
for the final classification \cite{DBLP:journals/corr/SantosXZ15}. In their work it is done by pairwise comparison. 
Moreover, this approach produces the matrix of relation embeddings as one of the outputs. 
Also, the authors were able to obtain natural language patterns that are most probable 
characteristics of the specific relation, e.g. ''\textbf{x} is part of \textbf{y}'' can be the pattern for 
relation \textbf{y}-contains-\textbf{x} (where \textbf{x} and \textbf{y} are named entities).

Taking into account the sequential structure of any textual data it also would be natural 
to suggest, that Recurrent Neural Networks will work well for Natural Language Processing tasks. Recurrent Neural Networks 
were the first ones to be used for machine translation for example. According to the 
overview of Relation Classification task in medical domain \cite{Luo05022016} the best 
solvers use information gained from the tree structure of the text after preprocessing it 
by NLP means. So \cite{DBLP:journals/corr/TaiSM15} suggested to transform neural network 
to capture this data better and class of tree-structured recurrent neural networks was applied to the task. 
Also, when working on Relation Classification it is rather important to take into account 
information before and after the current word. For this purpose, bidirectional recurrent neural networks 
are found to be useful \cite{DBLP:journals/corr/MiwaB16}.

An additional challenge in Relation Classification task is usually an extremely small amount 
of labeled training data. While the amount of training data is definitely one of the most 
important aspects of the successful application of deep learning models it is a very
important problem to be solved. One of the concepts for dealing with it is Distant Supervision
\cite{Mintz:2009:DSR:1690219.1690287}. It proposes to extract training examples 
from raw textual data using known relations from Knowledge Base. The results of distantly supervised learning might be improved 
by Multiple Instance Learning \cite{zeng2015distant}, that neglects possible mistakes 
when labelling training examples.

\section{Concepts and background}

\subsection{Natural Language Processing} 
The field of Natural Language Processing covers all the tasks about computer manipulation of natural 
languages, such as English, German and Russian. These languages are hard to work with automatically because they are hard to pin 
down with explicit rules \cite{bird2009natural}.

One of the tasks in the field of Natural Language Processing is Information Extraction. The amount of raw textual 
information in all the fields of human activity, for now, is truly impressive and it keeps growing 
every day. But modern Natural Language Processing still lacks optimal ways of exploiting information from huge volumes of texts. The task is extremely important because with its solution humans will 
get an easy way of answering any questions they have if it is possible to find the answer 
somewhere in the texts existing in available format. 

\subsection{Information Extraction} 
Information Extraction is concerned with building informational structures called Knowledge Base. Knowledge Base is usually 
represented as a graph structure, where every node will be some specific named entity and the 
edges are the relations that connect these entities. It is typically called knowledge graph.

Named entities can be of various kinds 
and forms. For a human, it is very easy to recognise that ''The Titanic'' and ''a 1997 American epic romance-disaster film directed, written, co-produced, and co-edited by James Cameron'' represent the 
same entity, while for a machine it is already a demanding task. So the nodes of a knowledge graph are 
very generic entities, which should be recognised in any text and in any form later in order to 
retrieve needed information automatically. This task is called Named Entity Recognition. It also 
includes disambiguation of entities. For example, with the entity ''Titanic'' machine should be 
able to distinguish between the ship itself and the movie and moreover recognise what exactly was mentioned in a text.

After the entities are recognised and all the mentions are found in the text the next step is to 
extract and classify relations between them. Basically, the task of Relation Extraction is about 
getting semantic meaning out of the sentences and texts that contain two entities mentions. 
This semantic meaning is later aligned with one of the pre-defined relations (so-called ''fixed schema'') 
or it can also be taken in its natural form as a new relation \cite{riedel2013relation}. This task 
is very hard to automatise because there is a variety of syntactic forms, that can represent the 
same semantic meaning and, on the other hand, phrases being very close syntactically can have different 
semantic meaning. Furthermore, there are a lot of sentences that do not describe any relation at all between the marked entities. 

Classical and the most used method for Relation Extraction is to get all possible linguistic 
characteristics of the raw textual data and then apply different kinds of classifiers on top of these constructed 
feature vectors. But as it was already mentioned, natural languages are very hard to organise 
into strict logical structures and this feature extraction is highly complicated and not 
very reliable, especially when the extraction should be performed on texts in different languages. 

Taking into account these 
considerations and the recent increase in computational power, deep neural networks got their place among methods for solving the task. Neural networks are extremely useful because they automatically extract and prioritise relevant features, without 
human supervision. The only modification needed is representing the text in less sparse form 
than human readable texts. The idea of word embeddings \cite{NIPS2013_5021} is exploited to obtain these representations. 

\subsection{Word Embeddings} 
Word embedding is a way of transforming human readable words to the real number vector 
space of the desired dimension. The transformation can, for example, be learned by a very simple neural network, that tries to predict words from its context \cite{NIPS2013_5021}. Embeddings 
can be learned from any sufficiently large volume of text data. There are propositions that embeddings  
as vectors containing latent features of words will differ if the corpora they are trained on are different \cite{bollegala2015unsupervised}. So for 
tasks in the medical domain it is better to use embeddings trained on medical texts. There are also other ways of obtaining embeddings besides neural networks, such 
as learning statistical information about occurrences of words in texts or mixed models incorporating the best of other methods. Three types of word embeddings were considered 
for experiments in this work.

\paragraph{Word2Vec} Word2Vec was introduced as an alternative method for obtaining 
vector representations of words. For that time most popular methods were Latent Semantic 
Analysis and Latent Dirichlet Allocation. In \cite{NIPS2013_5021} Neural Networks were 
suggested as a method for training word vectors. The idea behind is predicting either word from 
its context or context from the input word. It showed up to be efficient in terms of possibility to 
be trained on large text corpora for achieving higher accuracies. 

\paragraph{GloVe} GloVe was introduced in \cite{pennington2014glove} as a result of trying 
to make vector space representations of words more interpretable. It connects two ideas of 
embedding - usage of matrix factorisation and local context windows. Thus, it uses whole 
corpus words co-occurrences but also takes into account local context. The evaluation showed that 
this model outperforms other embeddings models of that time on the tasks of word analogy, 
Named Entity Recognition.

\paragraph{Swivel} Swivel is one of the latest models for obtaining word embeddings 
\cite{DBLP:journals/corr/ShazeerDEW16}. The idea behind this method is to factorise the whole 
matrix of co-occurrences constructed from the text corpora by dividing it into smaller parts and 
parallelising computations. The idea is taken both from GloVe model and skip-gram model 
(Word2Vec).

\subsection{Convolutional Neural Networks} An artificial neural network architecture that is widely used for extracting features is a Convolutional Neural Network. Convolutional Neural Networks 
(Figure \ref{fig:cnn}) are the backbone of the most modern Computer Vision 
systems today. The idea behind this type of networks is convolution, a sliding 
window function that can be applied to a matrix. The sliding window is called a kernel. 
Thus, application of a specific kernel will obtain values for different specific features described by 
this kernel. Convolutional Neural Network has one or more convolutions of different kernel sizes applied one by one to the 
input. After each convolution operation pooling is applied in order to squeeze the size 
of the processed data. This, in turn, allows  processing of varies different features, i.e. the width and the height 
gets smaller, while depth gets larger. After all the convolutional and pooling layers one or more fully-connected layers are added. A final activation function, e.g. 
softmax for classification tasks, is added at the end.

In the context of the Relation Classification task, Convolutional Neural Network gets 
a sentence with marked entities as an input and output the class of the relation described in it. So, 
instead of manual feature extraction and selection functionality of kernels and pooling is 
utilised.

	\begin{figure}
		\centering
		\includegraphics[width=\linewidth]{chapter2_background/images/cnn.png}
		\caption[Convolutional Neural Network architecture]{Example of a convolutional neural network, applied for image classification. Every ''convolutional'' layer applies convolution and pooling, thus obtaining values of the most critical features. They are further let through several fully-connected layers and classified (usually by softmax).}
		\label{fig:cnn}
	\end{figure}
	
\subsection{Distant Supervision} Deep neural networks, as they are trained in a supervised way, 
 require sufficient amount of labeled training data. For the Relation Classification task this data is 
 a set of sentences with two or more marked named entities. Relations represent knowledge 
 in different domains, so the construction of a sufficiently big training set requires work of experts in the 
 corresponding area. That is usually very costly, therefore various ways of eliminating this 
 problem are developed. One of them is Distant Supervision \cite{Mintz:2009:DSR:1690219.1690287} that exploits already existing knowledge about relations in form of a Knowledge Base.
 
 The assumption made by distantly supervised methods is that every sentence containing two 
 entities which are known to be in some relation according to the Knowledge Base will express exactly 
 that relation. In other words, when \textbf{e1} is related to \textbf{e2} as \textbf{r} and they both 
 are marked in a sentence, the sentence will be labeled as a training example for the relation 
 \textbf{r}. For example, if there is an entity pair \textit{''Laura Bush''} - \textit{''George W. Bush''} 
 with relation \textit{''spouse''} in the Knowledge Base, then examples of distantly supervised 
 training instances might be:
 
 \begin{itemize}
   \item "\textit{Laura Bush}, the wife of President-elect \textit{George W. Bush}, said in an interview broadcast today that she did not think that the 1973 supreme court decision declaring a woman's constitutional right to an abortion should be overturned."
   \item "\textit{Laura Bush} and other members of the Bush family were at Zahira's salon in the Watergate hotel on Thursday, and former President \textit{George W. Bush} was there yesterday for a haircut and facial.
 \end{itemize}
 
 Both of the sentences have two of the entities and thus they are assumed to describe 
 \textit{spouse} relation. However, it can be seen that in the case of the second sentence assumption 
 is wrong. That is the biggest flaw of Distant Supervision - the label might be wrong in most of the 
 extracted training examples. One more flaw introduced by the idea of Distant Supervision is 
 an assumption that every entity pair, that is not there in the Distant Supervision is not in the relation. So 
 the sentences with mentions of these pairs will be treated as negative examples, while 
Knowledge Base might be simply not complete.
 
 Both problems were tackled by various methods, such as attention mechanism 
 \cite{lin2016neural} or learning only from positive and unlabelled examples \cite{min2013distant}.
 
 \subsection{Multiple Instance Learning} To overcome the problem of wrong labelling, the assumption 
 made by Distant Supervision should be weakened. The way suggested in \cite{zeng2015distant} 
 is to use Multiple Instance Learning. This concept was introduced already in 90-s for drug 
 classification task \cite{dietterich1997solving}. Idea behind is about randomly uniting training 
 instances into so-called bags and labelling the whole bag according to the label of the instance 
 that gives the largest score. The high level algorithm of Multiple Instance Learning in the case when the 
 classification task is binary (i.e. there are two labels, 0 or 1) is provided in the Listing \ref{alg:multi-inst}.
 
 \begin{algorithm}[H]
 \paragraph{Data:}A set of bags $\{B_1, ..., B_n\}$ each with label $l_i \in \{0,1\}$. Each $B_i$ is a multiset of $n_i$ instances, $B_i = \{B_{i1}, ..., B_{in_i}\}$ .
 \paragraph{Constraints:}There exists a concept $c$ such that:
 \begin{itemize}
   \item For every $B_i$ with $l_i  = 1$, $c(B_{ij}) = 1$ for at least one $j$, and
   \item For every $B_i$ with $l_i = 0$, $c(B_{ij}) = 0$ for all $j$.
 \end{itemize}  
 \paragraph{Do:}Learn a concept that maps a bag $B_i$ to its label $l_i$.
 \caption{Statement of multiple instance classification problem, taken from \cite{ray2011multi}}
 \label{alg:multi-inst}
\end{algorithm}

 As it mentioned in \cite{ray2011multi}, Multiple Instance Learning was applied to Natural Language Processing task 
 of document classification. In this case, the whole document is considered to be a bag of 
 paragraphs and topic of the document is concluded from the maximal score topic of each its 
 paragraphs.
 
 Uniting training examples into bags and considering only maximal scored label helps to avoid 
 wrong labelling problem of Distant Supervision. Additionally using a small set of ''gold standard'' 
 data, labeled manually also can improve results achieved with distantly supervised training \cite{angeli2014combining}. 
 
 
	